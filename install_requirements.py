from __future__ import print_function
"""
Tool to install all needed packages out of a requirements.in or .txt

Activate your virtual environment and call:
    - python install_requirements.py

A requirements.in contains a list of packages with a specific version like:
    - coverage==5.4
    - opencv-python>=4.5
    - ...
You can also check the Python version by specifying:
    - #python==3.8 or
    - #python>=3.3 or
    - #python<=3.8
    - only main version is valid as well (like: #python==3)
    - Important: do not remove the hashtag before python!

See example here or at snippets
"""
import os
import sys


def check_python_version(version_string):
    """
    Checks if python version is expected version.

    Args:
        version_string: like: #python==3.8.6
    """
    current_main_version = sys.version_info[0]
    current_sub_version = sys.version_info[1]
    expected_main = 0
    expected_sub = 0

    symbols_to_check = ['==', '>=', '<=']
    symbol = None

    for symbol in symbols_to_check:
        if symbol in version_string:
            version_number = version_string.split(symbol)[-1]
            if '.' in version_number:
                expected_main, expected_sub = version_number.split('.')[:2]
            else:
                expected_main = version_number
                expected_sub = '-1'
            break

    if symbol is None:
        print(f'Could not extract any version information out of {version_string}. Using current python.')
        return

    try:
        expected_main = int(expected_main)
        expected_sub = int(expected_sub)
    except ValueError:
        print(f'Could not translate: {expected_main} or {expected_sub} to int. Skipping version check.')
        return

    raise_error = False
    error = None

    if symbol == '==':
        if expected_main != current_main_version or (expected_sub != current_sub_version and expected_sub != -1):
            raise_error = True
        if expected_sub == -1:
            expected_sub = 'x'

        error = (
            f'WARNING: You are using Python {current_main_version}.{current_sub_version} but expected version is: '
            f'Python {expected_main}.{expected_sub}!\n'
            f'Please use Python == {expected_main}.{expected_sub}!'
        )

    elif symbol == '>=':
        error = (
            f'WARNING: Your Python version is {current_main_version}.{current_sub_version} but should be '
            f'>= {expected_main}.{expected_sub}!\n'
            f'Please use any Python >= {expected_main}.{expected_sub}!'
        )
        if current_main_version < expected_main:
            if expected_sub < 0:
                expected_sub = 0
            raise_error = True
        if current_main_version == expected_main:
            if current_sub_version < expected_sub:
                raise_error = True

    elif symbol == '<=':
        error = (
            f'WARNING: The current Python version is {current_main_version}.{current_sub_version} but should be '
            f'<= {expected_main}.{expected_sub}!\n'
            f'Please use any Python <= {expected_main}.{expected_sub}!'
        )
        if current_main_version > expected_main:
            if expected_sub < 0:
                expected_sub = 0
            raise_error = True
        if current_main_version == expected_main:
            if current_sub_version > expected_sub:
                raise_error = True
    else:
        print('An unexpected error occurred. Please confirm the correct Python version.')

    if raise_error:
        raise RuntimeError(error)


def get_torch_package_string():
    if sys.platform.startswith('win32'):
        print('Installing pytorch for Windows with GPU support.')
        torch_package = 'pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio===0.9.0 ' \
                        '-f https://download.pytorch.org/whl/torch_stable.html'
    elif sys.platform.startswith('linux'):
        print('Installing pytorch for Linux with GPU support.')
        torch_package = 'pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio==0.9.0 ' \
                        '-f https://download.pytorch.org/whl/torch_stable.html'
    else:
        print('MacOS Binaries dont support CUDA, using CPU. Install from source if CUDA is needed')
        torch_package = 'pip install torch torchvision torchaudio'

    return torch_package


if __name__ == '__main__':
    current_dir = os.path.dirname(__file__)
    requirement_file = os.path.join(current_dir, 'requirements.in')
    requirement_txt_file = os.path.join(current_dir, 'requirements.txt')

    if not os.path.isfile(requirement_file):
        print('Please create a requirements.in file for easier packaging.')
        if not os.path.isfile(requirement_txt_file):
            if sys.version_info[0] < 3:
                sys.exit('Could not find neither requirements.in or requirements.txt')
            else:
                raise FileNotFoundError('Could not find neither requirements.in or requirements.txt')

        os.system('pip install -r requirements.txt')

    else:
        python_version = None
        packages = []
        with open(requirement_file, 'r') as file:
            for line in file:
                line = line.replace('\n', '')
                line.rstrip()
                if '#python' in line:
                    check_python_version(line)
                else:
                    if 'torch' in line or 'pytorch' in line:
                        install_torch = True
                        continue

                    packages.append(line)

        command = ' && '.join(['pip install "{}"'.format(package) for package in packages])

        if command:
            if install_torch:
                # chained pytorch installation fails somehow.
                pytorch_package = get_torch_package_string()
            else:
                pytorch_package = ''

            os.system('python -m pip install -U pip')
            os.system(command)

            if install_torch:
                os.system(pytorch_package)
        else:
            print('Could not find any packages to install.')

        # os.system('pip freeze>requirements.txt')
        print('Finished.')
