from pytorch_segmentation.dl_models import DenseVoxelNet
from pytorch_segmentation.tests.model_base_tests import ModelBaseTests3D


class DenseVoxelNetTests(ModelBaseTests3D):
    def test_forward(self):
        self.start_test('DenseVoxelNet', DenseVoxelNet)
