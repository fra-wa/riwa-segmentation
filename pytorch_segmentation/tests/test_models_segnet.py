from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import SegNet
from pytorch_segmentation.dl_models import SegResNet


class SegNetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('SegNet', SegNet)
        self.start_test('SegResNet', SegResNet)
