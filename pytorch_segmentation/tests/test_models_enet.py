from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import ENet


class ENetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('ENet', ENet)
