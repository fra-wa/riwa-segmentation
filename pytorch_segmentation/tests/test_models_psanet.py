from pytorch_segmentation.tests.model_base_tests import ModelBaseTests2D
from pytorch_segmentation.dl_models import PSANet


class PSANetTests(ModelBaseTests2D):
    def test_forward(self):
        self.start_test('PSANet', PSANet, backbone='resnet50', in_size=256)
