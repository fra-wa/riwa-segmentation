var RELOAD_INTERVAL = 3;  // seconds
var TIME_REMAINING = RELOAD_INTERVAL;
var countdown_running = false;
var TIMER;

var csrftoken;
var postKillURL;

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function postKillRequest() {
    document.getElementById("js-refresh-button").disabled = true;
    document.getElementById("js-kill-button").disabled = true;
    let killFormData = new FormData();
    killFormData.append('kill_process', 'true');
    let xhr = new XMLHttpRequest();
    xhr.open('POST', postKillURL, false);
    xhr.setRequestHeader('X-CSRFToken', csrftoken);
    xhr.send(killFormData);
}

// set initial refresh rate
function set_initial_refresh_rate() {
    let refresh_text = document.getElementById("js-refresh-time");
    let refresh_range = document.getElementById("js-refresh-range");

    if ( sessionStorage.getItem('reload_interval') != null ) {
        RELOAD_INTERVAL = parseInt(sessionStorage.getItem('reload_interval'));
    } else {
        sessionStorage.setItem('reload_interval', RELOAD_INTERVAL.toString());
    }

    if (RELOAD_INTERVAL === 61) {
        // for better chart viewing.
        RELOAD_INTERVAL = 120;
    }
    refresh_range.value = RELOAD_INTERVAL.toString();

    if (refresh_range.hasAttribute('disabled')) {
        refresh_text.innerHTML  = 'Process finished. Auto refresh disabled.';
    } else {
        refresh_text.innerHTML  = 'Refresh in ' + RELOAD_INTERVAL.toString() + ' / ' + RELOAD_INTERVAL.toString() + 'seconds.';
    }
}


function countdown() {
    let refresh_text = document.getElementById("js-refresh-time");
    let refresh_button = document.getElementById("js-refresh-button");
    countdown_running = true;
    if (TIME_REMAINING === -1) {
        clearTimeout(TIMER);
        window.location.reload();
    } else {
        refresh_text.innerHTML  = 'Refresh in ' + TIME_REMAINING.toString() + ' / ' + RELOAD_INTERVAL.toString() + ' seconds.';
        refresh_button.innerHTML  = 'Refresh (automatic in ' + TIME_REMAINING.toString() + ' seconds)';
        TIME_REMAINING--; //we subtract the second each iteration
    }
}


function start_count_down () {
    TIME_REMAINING = RELOAD_INTERVAL - 1;
    if (!countdown_running) {
        TIMER = setInterval(countdown, 1000); //set the countdown to every second
    }
}

// update refresh rate
function update_refresh_rate() {
    let refresh_text = document.getElementById("js-refresh-time");
    let refresh_range = document.getElementById("js-refresh-range");
    let refresh_button = document.getElementById("js-refresh-button");

    RELOAD_INTERVAL = parseInt(refresh_range.value);

    if (RELOAD_INTERVAL === 61) {
        RELOAD_INTERVAL = 120;
        // for better chart viewing.
    }

    sessionStorage.setItem('reload_interval', RELOAD_INTERVAL.toString());

    refresh_range.value = RELOAD_INTERVAL.toString();
    refresh_text.innerHTML  = 'Refresh in ' + RELOAD_INTERVAL.toString() + ' / ' + RELOAD_INTERVAL.toString() + ' seconds.';
    refresh_button.innerHTML  = 'Refresh (automatic in ' + RELOAD_INTERVAL.toString() + ' seconds)';
}


window.addEventListener('load', function load(event) {
    csrftoken = getCookie('csrftoken');

    let textArea = document.getElementById("log_data");
    let log_text = document.getElementsByClassName('js-data-holder')[0].dataset.log_text;
    let refresh_range = document.getElementById("js-refresh-range");

    postKillURL = document.getElementsByClassName('js-data-holder')[0].dataset.post_kill_url;

    textArea.value = log_text;
    // auto scroll down in text area
    textArea.scrollTop = textArea.scrollHeight;
    set_initial_refresh_rate();

    let kill_button = document.getElementById("js-kill-button");
    if (!kill_button.hasAttribute("disabled")) {

        kill_button.addEventListener("click", function () {
            postKillRequest();
        });

        refresh_range.addEventListener("input", function(event) {
            update_refresh_rate();
            start_count_down();
        });

        update_refresh_rate();
        start_count_down();
    }

});
