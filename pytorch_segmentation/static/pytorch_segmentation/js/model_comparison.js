var data_dict;
var chart_series = [];
var x_axis_categories = {};
var y_axis = {};
var chart_title = 'There are no models to analyze.';
var chart_spacing_left = 10;
var tooltip_data = '';
var sort_by_this_dataset_identifier = '';

var comparison_chart;

var hz_navbar_ids = [];
var vt_navbar_ids = [];
var arch_filter_buttons = [];

// slightly customized theme
function set_gray_theme() {
    Highcharts.theme = {
        colors: [
            '#7798BF', '#F19E53', '#8AC668', '#DA6D85',  // blue, orange like, green like, red like,
            '#8087E8', '#f45b5b', '#E1D369', '#A3EDBA',  // blue/purple, red, yellow, teal
            '#6699A1', '#F0A079', '#eeaaee', '#90ee7e',  // dark-gray-blue, orange, purple, light green
            '#87B4E7', '#BBDEE0', '#CB3F44', '#E3AF3D',  // lighter blue, light teal, red, yellow-orange
            '#6783E7', '#9630EF', '#BBBAC5', '#4DA9A5',  // blue, purple, gray, blue-teal
        ],
        chart: {
            backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                stops: [
                    [0, '#3e3e40'],
                    [1, '#2a2a2b']
                ]
            },
            style: {
                fontFamily: '\'Unica One\', sans-serif'
            },
            plotBorderColor: '#606063'
        },
        title: {
            style: {
                color: '#E0E0E3',
                fontSize: '20px'
            }
        },
        subtitle: {
            style: {
                color: '#84868b',
                textTransform: 'uppercase'
            }
        },
        xAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: '#E0E0E3'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            title: {
                style: {
                    color: '#A0A0A3'

                }
            }
        },
        yAxis: {
            gridLineColor: '#707073',
            labels: {
                style: {
                    color: '#E0E0E3'
                }
            },
            lineColor: '#707073',
            minorGridLineColor: '#505053',
            tickColor: '#707073',
            tickWidth: 1,
            title: {
                style: {
                    color: '#A0A0A3'
                }
            }
        },
        tooltip: {
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            style: {
                color: '#F0F0F0'
            }
        },
        plotOptions: {
            series: {
                dataLabels: {
                    color: '#F0F0F3',
                    style: {
                        fontSize: '13px'
                    }
                },
                marker: {
                    lineColor: '#333'
                }
            },
            boxplot: {
                fillColor: '#505053'
            },
            candlestick: {
                lineColor: 'white'
            },
            errorbar: {
                color: 'white'
            }
        },
        legend: {
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            itemStyle: {
                color: '#E0E0E3'
            },
            itemHoverStyle: {
                color: '#FFF'
            },
            itemHiddenStyle: {
                color: '#606063'
            },
            title: {
                style: {
                    color: '#C0C0C0'
                }
            }
        },
        credits: {
            style: {
                color: '#666'
            }
        },
        labels: {
            style: {
                color: '#707073'
            }
        },

        drilldown: {
            activeAxisLabelStyle: {
                color: '#F0F0F3'
            },
            activeDataLabelStyle: {
                color: '#F0F0F3'
            }
        },

        navigation: {
            buttonOptions: {
                symbolStroke: '#DDDDDD',
                theme: {
                    fill: '#505053'
                }
            }
        },

        // scroll charts
        rangeSelector: {
            buttonTheme: {
                fill: '#505053',
                stroke: '#000000',
                style: {
                    color: '#CCC'
                },
                states: {
                    hover: {
                        fill: '#707073',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    },
                    select: {
                        fill: '#000003',
                        stroke: '#000000',
                        style: {
                            color: 'white'
                        }
                    }
                }
            },
            inputBoxBorderColor: '#505053',
            inputStyle: {
                backgroundColor: '#333',
                color: 'silver'
            },
            labelStyle: {
                color: 'silver'
            }
        },

        navigator: {
            handles: {
                backgroundColor: '#666',
                borderColor: '#AAA'
            },
            outlineColor: '#CCC',
            maskFill: 'rgba(255,255,255,0.1)',
            series: {
                color: '#7798BF',
                lineColor: '#A6C7ED'
            },
            xAxis: {
                gridLineColor: '#505053'
            }
        },

        scrollbar: {
            barBackgroundColor: '#808083',
            barBorderColor: '#808083',
            buttonArrowColor: '#CCC',
            buttonBackgroundColor: '#606063',
            buttonBorderColor: '#606063',
            rifleColor: '#FFF',
            trackBackgroundColor: '#404043',
            trackBorderColor: '#404043'
        },
    };

    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);
}


function dataNeedsReverse(hz_navbar_id) {
    let keywords_to_search = ['loss', 'false_positives', 'false_negatives', 'storage_size', 'inference_time']

    for (let i = 0; i < keywords_to_search.length; i++) {
        if (hz_navbar_id.includes(keywords_to_search[i])) {
            return true;
        }
    }
    return false;
}


function setChartData(hz_navbar_id) {
    // highcharts sorting bugs out when having multiple different datasets. Somehow, the relation between x and y value
    // gets lost and the y-data does no more relate to its x-axis category (using only one dataset works but this is
    // not intended)

    // set up x axis depending on sorting or default order (given by backend)
    // each dataset contains same number of data points -> which we are using for creation if no sorting doesnt matter
    x_axis_categories = [];

    Object.entries(data_dict).forEach(([dataset_identifier, dataset_data]) => {
        if (dataset_identifier === sort_by_this_dataset_identifier) {
            let data = [];

            Object.entries(dataset_data).forEach(([architecture, architecture_data]) => {
                // first element is the name --> type string
                if (typeof(architecture_data) === 'object') {
                    let architecture_is_active = document.getElementById(architecture_data['dom_element_id']).className.split(' ').includes('active');

                    if (architecture_is_active) {
                        data.push([architecture, architecture_data[hz_navbar_id]]);
                    }
                }
            });

            let sorted_array = data.sort(function(a, b) { return b[1] - a[1]; });

            if (dataNeedsReverse(hz_navbar_id)) {
                sorted_array = sorted_array.reverse();
            }

            // top x networks
            let best_x;
            let top_x_button = document.getElementById('js-top-x');
            let top_x_input = document.getElementById('js-top-x-input');
            if (top_x_button.className.split(' ').includes('active') && top_x_input.className.split(' ').includes('is-valid')) {
                best_x = parseInt(top_x_input.value);
            } else {
                best_x = sorted_array.length;
            }

            if (sorted_array.length < best_x) {
                best_x = sorted_array.length;
            }

            for (let i = 0; i < best_x; i++) {
                x_axis_categories.push(sorted_array[i][0]);
            }

        } else if (sort_by_this_dataset_identifier === '' && x_axis_categories.length === 0) {
            let data = [];

            Object.entries(dataset_data).forEach(([architecture, architecture_data]) => {
                if (typeof(architecture_data) === 'object') {
                    let architecture_is_active = document.getElementById(architecture_data['dom_element_id']).className.split(' ').includes('active');
                    if (architecture_is_active) {
                        data.push([architecture, architecture_data[hz_navbar_id]]);
                    }
                }
            });

            for (let i = 0; i < data.length; i++) {
                x_axis_categories.push(data[i][0]);
            }
        }
    });

    // set x axis rotation if too many architectures are active
    let rotation_angle = 0;
    let font_size = '13px';

    let active_architectures_count = x_axis_categories.length;

    if (active_architectures_count > 60) {
        rotation_angle = -90;
        font_size = '8px';
    } else if (active_architectures_count > 50) {
        rotation_angle = -90;
        font_size = '9px';
    } else if (active_architectures_count > 45) {
        rotation_angle = -90;
        font_size = '10px';
    } else if (active_architectures_count > 40) {
        rotation_angle = -90;
        font_size = '12px';
    } else if (active_architectures_count > 20) {
        rotation_angle = -90;
        font_size = '12px';
    } else if (active_architectures_count > 15) {
        rotation_angle = -70;
        font_size = '13px';
    } else if (active_architectures_count > 10) {
        rotation_angle = -45;
        font_size = '13px';
    } else if (active_architectures_count > 5) {
        rotation_angle = -22;
        font_size = '13px';
    }

    // style for x_axis_labels
    let x_axis_labels = {
        rotation: rotation_angle,
        style: {
            fontSize: font_size,
            fontFamily: 'Helvetica'
        }
    };

    chart_series = [];
    let accuracy_min_value = 101;

    Object.entries(data_dict).forEach(([dataset_identifier, dataset_data]) => {
        let data = [];

        // Set up x axis related data
        for (let i = 0; i < x_axis_categories.length; i++) {
            let architecture_data = dataset_data[x_axis_categories[i]];
            let architecture_is_active = document.getElementById(architecture_data['dom_element_id']).className.split(' ').includes('active');
            if (architecture_is_active) {
                data.push(architecture_data[hz_navbar_id]);
                if (document.getElementById(hz_navbar_id).dataset.data_unit === '%' && architecture_data[hz_navbar_id] != null && architecture_data[hz_navbar_id] < accuracy_min_value) {
                    accuracy_min_value = architecture_data[hz_navbar_id];
                }
            }
        }

        // Set up series data
        let series_data = {
            name: dataset_data['readable_name'],
            data: data,
            dataSorting: {
                enabled: false,
                matchByName: false,
            },
        };

        chart_series.push(series_data);

    });

    // set up chart axes and title
    let y_axis_unit = document.getElementById(hz_navbar_id).dataset.data_unit;
    chart_title = document.getElementById(hz_navbar_id).innerHTML;

    if (accuracy_min_value === 101) {
        // no nothing is active
        accuracy_min_value = 0;
    } else {
        accuracy_min_value = Math.floor((accuracy_min_value / 10)) * 10;
        if (accuracy_min_value === 100) {
            accuracy_min_value = 90;
        }
    }

    if (y_axis_unit === "%") {
        y_axis = {
            title: {
                text: 'Accuracy in %',
            },
            max: 100,
            min: accuracy_min_value,
        };

        tooltip_data = {
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> {point.y:.2f} %</b></td></tr>',
        };
    } else if (y_axis_unit === "s") {
        y_axis = {
            title: {
                text: 'Time in s',
            },
            min: null,
            max: null,
        };

        tooltip_data = {
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> {point.y:.1f} s</b></td></tr>',
        };

    } else if (y_axis_unit === "MB") {
        y_axis = {
            title: {
                text: 'Storage size in MB',
            },
            min: null,
            max: null,
        };

        tooltip_data = {
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> {point.y:.1f} MB</b></td></tr>',
        };

    } else {
        let y_title, y_max, y_min;
        if (y_axis_unit === "loss") {
            y_title = 'Loss';
            y_max = null;
            y_min = 0;
        } else if (y_axis_unit === "zero_to_one") {
            y_title = '';
            y_max = 1;
            y_min = 0;
        } else {
            y_title = '';
            y_max = null;
            y_min = null;
        }

        y_axis = {
            title: {
                text: y_title,
            },
            min: y_min,
            max: y_max,
        };

        tooltip_data = {
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b> {point.y:.3f}</b></td></tr>',
        };
    }

    comparison_chart.update({
        series: chart_series,
        title: {text: chart_title},
        xAxis: {
            categories: x_axis_categories,
            title: {
                text: 'Architecture backbone',
            },
            labels: x_axis_labels,
            crosshair: true,
        },
        yAxis: y_axis,
        tooltip: tooltip_data,
    }, true, true)
    comparison_chart.redraw();
}


function createChart(hz_navbar_id) {
    comparison_chart = Highcharts.chart('js-comparison-chart', {
        chart: {
            type: 'column',
        },
        title: {
            text: chart_title,
        },
        xAxis: {
            categories: x_axis_categories,
            title: {
                text: 'Architectures (backbone)',
            },
            labels: {},
            crosshair: true,
        },
        yAxis: y_axis,
        tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b> {point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true,
            },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: chart_series,
    });

    setChartData(hz_navbar_id);
}


function createHorizontalNavbarMagic() {
    // create initial chart, listeners and set/remove active
    for (let i = 0; i < hz_navbar_ids.length; i++) {
        let hz_element = document.getElementById(hz_navbar_ids[i]);

        if (hz_element) {
            // add listener
            hz_element.addEventListener("click", function () {
                setChartData(this.id);

                // remove active
                for (let j = 0; j < hz_navbar_ids.length; j++) {

                    let hz_element_to_remove_active = document.getElementById(hz_navbar_ids[j]);
                    if (hz_element_to_remove_active.className.split(' ').includes('active')) {
                        hz_element_to_remove_active.className = hz_element_to_remove_active.className.replace(' active', '');
                        break;
                    }
                }

                // set active
                this.className = this.className + ' active';
            });

            // create initial chart
            if (hz_element.className.split(' ').includes('active')) {
                createChart(hz_element.id);
            }
        }
    }
}


function createVerticalNavbarMagic() {
    for (let i = 0; i < vt_navbar_ids.length; i++) {
        let vt_element = document.getElementById(vt_navbar_ids[i]);

        if (vt_element) {
            vt_element.addEventListener("click", function () {

                // remove or set active
                if (this.className.split(' ').includes('active')) {
                    this.className = this.className.replace(' active', '');
                } else {
                    this.className = this.className + ' active';
                }

                // remove active from filter buttons
                for (let j = 0; j < arch_filter_buttons.length; j++) {
                    let btn_to_deactivate = arch_filter_buttons[j];
                    if (btn_to_deactivate.className.split(' ').includes('active')) {
                        btn_to_deactivate.className = btn_to_deactivate.className.replace(' active', '');
                    }
                }

                // update chart
                for (let j = 0; j < hz_navbar_ids.length; j++) {
                    if (document.getElementById(hz_navbar_ids[j]).className.split(' ').includes('active')) {
                        setChartData(hz_navbar_ids[j]);
                        break;
                    }
                }
            });
        }

    }
}


function activateOrDeactivateArchitectures(dimension_to_search='', activate_all=false, deactivate_all=false, dataset_identifier='') {
    if (activate_all) {
        for (let i = 0; i < vt_navbar_ids.length; i++) {
            let vt_element = document.getElementById(vt_navbar_ids[i]);
            if (!vt_element.className.split(' ').includes('active')) {
                vt_element.className = vt_element.className + ' active';
            }
        }

    } else if (deactivate_all) {

        for (let i = 0; i < vt_navbar_ids.length; i++) {
            let vt_element = document.getElementById(vt_navbar_ids[i]);
            if (vt_element.className.split(' ').includes('active')) {
                vt_element.className = vt_element.className.replace(' active', '');
            }
        }

    } else if (dimension_to_search !== '') {
        for (let i = 0; i < vt_navbar_ids.length; i++) {
            let vt_element = document.getElementById(vt_navbar_ids[i]);
            if (vt_element.dataset.dimension === dimension_to_search) {
                if (!vt_element.className.split(' ').includes('active')) {
                     vt_element.className = vt_element.className + ' active';
                }
            } else {
                if (vt_element.className.split(' ').includes('active')) {
                     vt_element.className = vt_element.className.replace(' active', '');
                }
            }
        }
    } else if (dataset_identifier !== '') {
        for (let i = 0; i < vt_navbar_ids.length; i++) {
            let vt_element = document.getElementById(vt_navbar_ids[i]);
            if (vt_element.hasAttribute('data-dataset_filter_' + dataset_identifier)) {
                if (!vt_element.className.split(' ').includes('active')) {
                     vt_element.className = vt_element.className + ' active';
                }
            } else {
                if (vt_element.className.split(' ').includes('active')) {
                     vt_element.className = vt_element.className.replace(' active', '');
                }
            }
        }
    } else {
        console.log('failed at activateOrDeactivateArchitectures: no parameter set')
    }

}


function createVerticalFilterMagic() {
    for (let i = 0; i < arch_filter_buttons.length; i++) {
        let button = arch_filter_buttons[i];
        button.addEventListener("click", function () {
            // deactivate all others and set this active
            for (let j = 0; j < arch_filter_buttons.length; j++) {
                if (arch_filter_buttons[j].className.split(' ').includes('active')) {
                    arch_filter_buttons[j].className = arch_filter_buttons[j].className.replace(' active', '');
                }
            }
            if (!this.className.split(' ').includes('active')) {
                this.className = this.className + ' active';
            }

            // activate/deactivate architectures
            if (this.id === 'js-show-2D') {
                activateOrDeactivateArchitectures('2D', false, false, '');
            } else if (this.id === 'js-show-3D') {
                activateOrDeactivateArchitectures('3D', false, false, '');
            } else if (this.id === 'js-show-all') {
                activateOrDeactivateArchitectures('', true, false, '');
            } else if (this.id === 'js-hide-all') {
                activateOrDeactivateArchitectures('', false, true, '');
            } else if (this.dataset.dataset_filter_button) {
                // filter by dataset
                activateOrDeactivateArchitectures('', false, false, this.dataset.dataset_identifier);
            }

            // update chart
            for (let j = 0; j < hz_navbar_ids.length; j++) {
                if (document.getElementById(hz_navbar_ids[j]).className.split(' ').includes('active')) {
                    setChartData(hz_navbar_ids[j]);
                    break;
                }
            }

        });
    }

}


function createSortingByDatasetMagic(dataset_identifiers) {
    let sort_buttons = [];
    for (let i = 0; i < dataset_identifiers.length; i++) {
        let sort_button_id = 'js-sort-by-' + dataset_identifiers[i];
        sort_buttons.push(document.getElementById(sort_button_id));
    }

    for (let i = 0; i < sort_buttons.length; i++) {
        sort_buttons[i].addEventListener("click", function () {
            // deactivate sorting
            let set_this_active = true;
            if (this.className.split(' ').includes('active')) {
                sort_by_this_dataset_identifier = '';
                set_this_active = false;

                // disable show only top x
                let top_x_button = document.getElementById('js-top-x');
                let top_x_input = document.getElementById('js-top-x-input');
                if (!top_x_button.hasAttribute('disabled')) {
                    top_x_button.setAttribute('disabled', '');
                }
                if (!top_x_input.readOnly) {
                    $("#js-top-x-input").prop("readonly", true);
                }
            }

            // deactivate all sort buttons
            for (let j = 0; j < sort_buttons.length; j++) {
                if (sort_buttons[j].className.split(' ').includes('active')) {
                    sort_buttons[j].className = sort_buttons[j].className.replace(' active', '');
                }
            }
            // activate this if wanted
            if (set_this_active) {
                this.className = this.className + ' active';
                sort_by_this_dataset_identifier = this.dataset.dataset_identifier;

                // enable show only top x
                let top_x_button = document.getElementById('js-top-x');
                let top_x_input = document.getElementById('js-top-x-input');
                if (top_x_button.hasAttribute('disabled')) {
                    top_x_button.removeAttribute('disabled');
                }
                if (top_x_input.readOnly) {
                    // using jquery here, javascript somehow does not work
                    $("#js-top-x-input").prop("readonly", false);
                }
            }

            // update chart
            for (let j = 0; j < hz_navbar_ids.length; j++) {
                if (document.getElementById(hz_navbar_ids[j]).className.split(' ').includes('active')) {
                    setChartData(hz_navbar_ids[j]);
                    break;
                }
            }
        });
    }
}


function createTopXMagic() {
    let top_x_button = document.getElementById('js-top-x');
    let top_x_input = document.getElementById('js-top-x-input');

    // button activating and deactivating
    top_x_button.addEventListener("click", function () {

        // remove or set active
        if (this.className.split(' ').includes('active')) {
            this.className = this.className.replace(' active', '');
        } else {
            this.className = this.className + ' active';
        }

        if (this.className.split(' ').includes('active')) {

            // remove validation classes
            if (top_x_input.className.split(' ').includes('is-valid')) {
                top_x_input.className = top_x_input.className.replace(' is-valid', '');
            }
            if (top_x_input.className.split(' ').includes('is-invalid')) {
                top_x_input.className = top_x_input.className.replace(' is-invalid', '');
            }

            // validate
            if (!top_x_input.value) {
                top_x_input.className += ' is-valid';
                top_x_input.value = 5;
            } else if (parseInt(top_x_input.value) <= 0) {
                top_x_input.className += ' is-invalid';
                top_x_input.value = '';
                this.className = this.className.replace(' active', '');
            } else if (parseInt(top_x_input.value) > top_x_input.dataset.max_value) {
                top_x_input.className += ' is-invalid';
                top_x_input.value = '';
                this.className = this.className.replace(' active', '');
            } else {
                top_x_input.className += ' is-valid';
            }

            if (top_x_input.className.split(' ').includes('is-valid')) {
                this.innerHTML = 'Show selected';
            }

        } else {
            this.innerHTML = 'Only top:';
            if (top_x_input.className.split(' ').includes('is-valid') || top_x_input.className.split(' ').includes('is-invalid')) {
                top_x_input.className = top_x_input.className.replace(' is-valid', '');
                top_x_input.className = top_x_input.className.replace(' is-invalid', '');
            }
        }

        // update chart
        for (let j = 0; j < hz_navbar_ids.length; j++) {
            if (document.getElementById(hz_navbar_ids[j]).className.split(' ').includes('active')) {
                setChartData(hz_navbar_ids[j]);
                break;
            }
        }

    });

    // top_x_input on change if active --> readonly is set so if user can set input, button is active -> no check needed
    top_x_input.addEventListener("keydown", ({key}) => {
        if (key === "Enter") {

            // remove validation classes
            if (top_x_input.className.split(' ').includes('is-valid')) {
                top_x_input.className = top_x_input.className.replace(' is-valid', '');
            }
            if (top_x_input.className.split(' ').includes('is-invalid')) {
                top_x_input.className = top_x_input.className.replace(' is-invalid', '');
            }

            // validate
            if (!top_x_input.value) {
                top_x_input.className += ' is-valid';
                top_x_input.value = 5;
            } else if (parseInt(top_x_input.value) <= 0) {
                top_x_input.className += ' is-invalid';
                top_x_input.value = '';
                top_x_button.className = top_x_button.className.replace(' active', '');
            } else if (parseInt(top_x_input.value) > top_x_input.dataset.max_value) {
                top_x_input.className += ' is-invalid';
                top_x_input.value = '';
                top_x_button.className = top_x_button.className.replace(' active', '');
            } else {
                top_x_input.className += ' is-valid';
            }
            // update chart
            if (top_x_input.className.split(' ').includes('is-valid')) {
                for (let j = 0; j < hz_navbar_ids.length; j++) {
                    if (document.getElementById(hz_navbar_ids[j]).className.split(' ').includes('active')) {
                        setChartData(hz_navbar_ids[j]);
                        break;
                    }
                }
            }
        }
    });
}


window.addEventListener('load', function load(event) {
    let data_holder = document.getElementsByClassName('js-data-holder')[0];
    data_dict = JSON.parse(data_holder.dataset.data_dict);

    let dataset_identifiers = Object.keys(data_dict);

    // prevent from already declared error during loops
    let i = 0;
    let j = 0;

    hz_navbar_ids = JSON.parse(data_holder.dataset.hz_navbar_ids);
    vt_navbar_ids = JSON.parse(data_holder.dataset.vt_navbar_ids);

    let only_2d = document.getElementById('js-show-2D');
    let only_3d = document.getElementById('js-show-3D');
    let show_all = document.getElementById('js-show-all');
    let hide_all = document.getElementById('js-hide-all');

    arch_filter_buttons = [only_2d, only_3d, show_all, hide_all];
    for (let i = 0; i < dataset_identifiers.length; i++) {
        let dataset_id = 'js-filter-by-' + dataset_identifiers[i];
        arch_filter_buttons.push(document.getElementById(dataset_id));
    }


    // Todo: filter 2 and 3d: https://www.geeksforgeeks.org/hide-or-show-elements-in-html-using-display-property/#:~:text=To%20hide%20an%20element%2C%20set,style.
    // inner html: change to Show 2D or Hide 2D

    set_gray_theme();

    // sets up initial graph and main domain selector
    createHorizontalNavbarMagic();

    // set up event listener for only 2d, only 3d, all, hide and datasets
    createVerticalNavbarMagic();

    // set filter functionality of architecture navigation
    createVerticalFilterMagic();

    // set sorting functionality
    createSortingByDatasetMagic(dataset_identifiers);

    // top x networks
    createTopXMagic();

});
