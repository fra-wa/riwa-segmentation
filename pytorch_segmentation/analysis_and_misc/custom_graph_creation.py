"""
Use this to create custom high quality graphs out of the database trained models.
See at bottom
"""
import math
import numpy as np
import os
import sys
import matplotlib

matplotlib.use('TkAgg')
from matplotlib import pyplot as plt

current_dir = os.path.dirname(os.path.abspath(__file__))
module_dir = os.path.dirname(os.path.dirname(current_dir))
sys.path.extend([module_dir])

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

import django
django.setup()

from pytorch_segmentation.models import ModelAnalysis

font = {
    'size': 28,  # general font size - used for ticks at y and y
    'family': 'Times New Roman',
}

matplotlib.rc('font', **font)


def ceil(number, decimals):
    """
    rounds up to a given decimal

    Args:
        number: to round
        decimals: decimal position; can be negative --> ceil(1025, -1) -> 1030

    Returns: rounded up number

    """

    return math.ceil((10.0 ** decimals) * number) / (10.0 ** decimals)


def floor(number, decimals):
    """
    rounds down to a given decimal

    Args:
        number: to round
        decimals: decimal position; can be negative --> ceil(1025, -1) -> 1020

    Returns: rounded up number

    """

    return math.floor((10.0 ** decimals) * number) / (10.0 ** decimals)


def round_to_base_number(x, base=5):
    """Example: round_to_number(12, 5) = 10; round_to_number(13, 5) = 15"""
    return base * round(x/base)


def save_plot(file_path,
              x_axis,
              y_data,
              colors,
              legend,
              legend_loc,
              x_label,
              y_label,
              x_lim,
              y_lim,
              major_ticks_y,
              minor_ticks_y,
              major_ticks_x=None,
              minor_ticks_x=None,
              title=None,
              ):
    fig, ax = plt.subplots(figsize=(10, 5))

    for i, y_values in enumerate(y_data):
        ax.plot(x_axis, y_values, color=colors[i], linewidth=4)

    params = {'mathtext.default': 'regular'}
    plt.rcParams.update(params)
    ax.legend(legend, loc=legend_loc, fontsize=32)
    if title is not None:
        plt.title(title, fontsize=34)
    plt.xlabel(x_label, fontsize=30)
    plt.ylabel(y_label, fontsize=30)
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlim(x_lim[0], x_lim[1])

    if major_ticks_x is None:
        major_ticks_x = np.arange(0, x_axis[-1], 5)
    if minor_ticks_x is None:
        minor_ticks_x = np.arange(0, x_axis[-1], 1)

    ax.set_xticks(major_ticks_x)
    ax.set_xticks(minor_ticks_x, minor=True)

    ax.set_yticks(major_ticks_y)
    ax.set_yticks(minor_ticks_y, minor=True)

    # set opacity of those ticks
    ax.grid(which='minor', alpha=0.5, linestyle='--')
    ax.grid(which='major', alpha=1, linestyle='-')

    # aspect_ratio_y_to_x = 0.25
    # if isinstance(y_data, list):
    #     ax.set_aspect(aspect_ratio_y_to_x * len(x_axis))
    # else:
    #     ax.set_aspect(aspect_ratio_y_to_x * x_axis.shape[0])

    # show grid
    plt.grid(True, which='both')
    plt.tight_layout()

    plt.savefig(file_path, dpi=400)
    file_path = os.path.join(os.path.dirname(file_path), os.path.basename(file_path).split('.')[0] + '.pdf')
    plt.savefig(file_path)
    plt.close()


def _get_base_5_tick(tick_size_string):
    if not isinstance(tick_size_string, str):
        raise ValueError('_get_base_5_tick expects a string.')

    if float(tick_size_string).is_integer():
        tick_size_tmp = float(tick_size_string)
        tick_size_tmp = round_to_base_number(tick_size_tmp, base=5)
        if tick_size_tmp != 0:
            return tick_size_tmp
        else:
            return float(tick_size_string)

    number, decimals = tick_size_string.split('.')[:]
    number = int(number)

    leading_decimal_zeroes = len(decimals) - len(str(int(decimals)))

    tmp_decimals = int(decimals)
    tmp_decimals = round_to_base_number(tmp_decimals, base=5)

    if tmp_decimals == 0 and number == 0:
        return int(decimals) / 10

    if tmp_decimals == 10**len(decimals):
        number += 1
        tmp_decimals = 0

    return float(f'{number}.{leading_decimal_zeroes * "0"}{tmp_decimals}')


def get_tick_sizes(data_lim_min, data_lim_max, expected_major_ticks=4, expected_minor_ticks=None, round_to_next_5=True):
    """

    Args:
        data_lim_min:
        data_lim_max:
        expected_major_ticks:
        expected_minor_ticks:
        round_to_next_5: if True: example: tick size = 0.22 --> tick size = 0.2; tick size = 0.23 --> tick size = 0.25

    Returns:

    """
    difference = data_lim_max - data_lim_min
    major_tick_size = difference / expected_major_ticks
    position_to_round = 1 + _get_position_to_add(major_tick_size)
    major_tick_size = round(major_tick_size, position_to_round)

    if expected_minor_ticks is None:
        minor_tick_size = round(major_tick_size / 2, position_to_round + 1)
    else:
        minor_tick_size = difference / expected_minor_ticks
        position_to_round = 1 + _get_position_to_add(minor_tick_size)
        minor_tick_size = round(minor_tick_size, position_to_round)

    if round_to_next_5:  # either 0, 5 or 10 --> 18 -> 20; 0.018 -> 0.02; 0.1 -> 0.1; 0.11 -> 0.1
        divided = 0
        while major_tick_size > 100:
            major_tick_size /= 10
            divided += 1
        if divided:
            major_tick_size = round(major_tick_size)
        major_tick_size_string = str(float(major_tick_size))
        major_tick_size = _get_base_5_tick(major_tick_size_string)
        major_tick_size = 10**divided * major_tick_size
        minor_tick_size = major_tick_size / 2

    return major_tick_size, minor_tick_size


def _get_position_to_add(number):
    """Used for get_y_lim"""
    if number < 0:
        raise ValueError('Number must be larger than 0')
    if 0 < number < 1:
        factor = 1
        val = number
        while val < 1:
            factor *= 10
            val = val * factor

        position_to_add = len(str(factor)) - 1
    else:
        divisor = 1
        val = number
        while val > 1:
            divisor *= 10
            val = val / divisor
        # why -2? example: 123 --> 3 is pos 0, 2 is pos -1, 1 is pos -2 --> we want 130 not 200
        position_to_add = -1 * (len(str(divisor)) - 2)
    return position_to_add


def get_y_lim(y_data, fix_min_to_zero=True):
    """
    Gets y axis limitations.
    Example:
        1.:
            y_data = [-123, -10, 1, 2]
            limits will be:
            y_min = -130
            y_max = 10
        2.:
            y_data = [87, 91, 120]
            limits will be:
            y_min = 80
            y_max = 130

    Args:
        y_data:
        fix_min_to_zero: you can set y min to zero if wanted. Useful at loss

    Returns: min value, max value
    """
    if not isinstance(y_data, (list, np.ndarray)):
        raise ValueError('y_data must be a list or numpy nd array')
    if not isinstance(y_data, np.ndarray):
        y_data = np.array(y_data)

    y_data[y_data is None] = np.nan

    max_val = np.nanmax(y_data)
    min_val = np.nanmin(y_data)

    max_is_negative = False
    if max_val < 0:
        max_val *= -1
        max_is_negative = True

    min_is_negative = False
    if min_val < 0:
        min_val *= -1
        min_is_negative = True

    decimal_to_add = _get_position_to_add(max_val)
    decimal_to_add_min = _get_position_to_add(min_val) + 1

    decimal_to_add = min(decimal_to_add_min, decimal_to_add)
    max_val = -1 * max_val + 1 / 10 ** decimal_to_add if max_is_negative else max_val + 1 / 10 ** decimal_to_add
    max_val = floor(max_val, decimal_to_add)

    if fix_min_to_zero:
        return 0, max_val

    min_val = -1 * min_val - 1 / 10 ** decimal_to_add if min_is_negative else min_val - 1 / 10 ** decimal_to_add
    min_val = ceil(min_val, decimal_to_add)
    if min_val < 0:
        min_val = 0

    return min_val, max_val


if __name__ == '__main__':
    out_path = r'Z:\TP-D03\99_Temporary\documents\SFB\2022\Herbsttreffen November'

    # city_no_aug = ModelAnalysis.objects.get(dataset__name='Cityscapes', custom_name='No augmentation')
    # city_augmented = ModelAnalysis.objects.get(dataset__name='Cityscapes Augmented', custom_name='')
    # city_online_aug = ModelAnalysis.objects.get(dataset__name='Cityscapes', custom_name='Policy geometric first n=8')
    #
    # kiwa_no_aug = ModelAnalysis.objects.get(dataset__name='KIWA', custom_name='No augmentation')
    # kiwa_augmented = ModelAnalysis.objects.get(dataset__name='KIWA Augmented', custom_name='')
    # kiwa_online_aug = ModelAnalysis.objects.get(dataset__name='KIWA', custom_name='Policy geometric only')

    fiber_no_aug = ModelAnalysis.objects.get(dataset__name='PE Fibers', custom_name='No Augmentation')
    fiber_augmented = ModelAnalysis.objects.get(dataset__name='PE Fibers', custom_name='Offline Augmentation')
    fiber_online_aug = ModelAnalysis.objects.get(dataset__name='PE Fibers', custom_name='Online Augmentation')

    analyses = [
        # city_no_aug,
        # city_augmented,
        # city_online_aug,
        # kiwa_no_aug,
        # kiwa_augmented,
        # kiwa_online_aug,
        fiber_no_aug,
        fiber_augmented,
        fiber_online_aug,
    ]

    for i, analysis in enumerate(analyses):
        header, history = analysis.get_history()

        if i < 3:
            history = history[:100]
        else:
            history = history[:200]

        history = np.array(history)
        history[history == None] = np.nan
        history = history.astype(np.float64)

        y_loss_range = get_y_lim(np.hstack((history[0:, 1], history[0:, 2])), fix_min_to_zero=True)
        major_tick_size, minor_tick_size = get_tick_sizes(0.0, 1)

        major_tick_size_x, minor_tick_size_x = get_tick_sizes(history[0, 0], history[-1, 0])

        if analysis.custom_name:
            loss_path = os.path.join(out_path, f'Loss {analysis.dataset.name}_{analysis.custom_name.replace("=", "_")}.png')
            title = analysis.custom_name.replace(
                "Policy ", "Online Augmentation ("
            ).replace(
                "geometric only", 'geo only)'
            ).replace(
                'geometric first n=8', 'geo first n=8)'
            ).replace('augmentation', 'Augmentation')
        else:
            loss_path = os.path.join(out_path, f'Loss {analysis.dataset.name}.png')
            title = 'Offline Augmentation'

        save_plot(
            file_path=loss_path,
            x_axis=history[:, 0],
            y_data=[history[:, 1], history[:, 2]],
            colors=['tab:olive', 'tab:blue'],
            legend=[r'$Training\/Loss$', r'$Validation\/Loss$'],
            legend_loc='upper left',
            x_label='Epoch',
            y_label='Loss',
            x_lim=[0, history[-1, 0]],
            y_lim=[0, 0.8],
            major_ticks_y=np.arange(0, 0.81, 0.2),
            minor_ticks_y=np.arange(0, 0.81, 0.1),
            major_ticks_x=np.arange(0, history[-1, 0] + 1, major_tick_size_x),
            minor_ticks_x=np.arange(0, history[-1, 0] + 1, minor_tick_size_x),
            title=title,
        )
