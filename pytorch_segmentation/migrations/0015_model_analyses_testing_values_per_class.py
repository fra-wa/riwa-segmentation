import numpy as np
from django.db import migrations

from pytorch_segmentation.file_handling.utils import convert_binary_to_np_array, convert_np_array_to_binary


def forwards(apps, schema_editor):
    analysis_model_class = apps.get_model('pytorch_segmentation', 'ModelAnalysis')
    analysis_qs = analysis_model_class.objects.filter(classes=2)
    for analysis in analysis_qs:
        if analysis.classes == 2 and analysis.testing_true_positives:
            all_pixels = analysis.testing_all_pixels
            true_positives = analysis.testing_true_positives
            true_negatives = analysis.testing_true_negatives
            false_positives = analysis.testing_false_positives
            false_negatives = analysis.testing_false_negatives
            true_positives_ratio = analysis.testing_true_positives_ratio
            true_negatives_ratio = analysis.testing_true_negatives_ratio
            false_positives_ratio = analysis.testing_false_positives_ratio
            false_negatives_ratio = analysis.testing_false_negatives_ratio
            precision = analysis.testing_precision
            mean_iou = analysis.testing_mean_iou
            dice_coefficient = analysis.testing_dice_coefficient

            all_background = true_negatives + false_positives
            all_foreground = true_positives + false_negatives

            testing_class_suited_for_calculation = np.array([1, 1], dtype=int)
            testing_pixels_per_class = np.array(
                [all_pixels - all_background, all_pixels - all_foreground], dtype=np.uint32
            )
            testing_accuracy_per_class = np.array(
                [true_negatives / all_background, true_positives / all_foreground]
            )
            background_tp = true_negatives
            background_fp = false_negatives
            background_fn = false_positives

            background_tp_ratio = true_negatives / all_background
            background_tn_ratio = true_positives / all_foreground
            background_fp_ratio = false_negatives / all_background
            background_fn_ratio = false_positives / all_foreground

            testing_true_positives_ratio_per_class = np.array([background_tp_ratio, true_positives_ratio])
            testing_true_negatives_ratio_per_class = np.array([background_tn_ratio, true_negatives_ratio])
            testing_false_positives_ratio_per_class = np.array([background_fp_ratio, false_positives_ratio])
            testing_false_negatives_ratio_per_class = np.array([background_fn_ratio, false_negatives_ratio])

            background_precision = background_tp / (background_tp + background_fp)
            background_dice = 2 * background_tp / (2 * background_tp + background_fp + background_fn)
            background_mean_iou = background_dice / (2 - background_dice)

            testing_precision_per_class = np.array([background_precision, precision])
            testing_mean_iou_per_class = np.array([background_mean_iou, mean_iou])
            testing_dice_coefficient_per_class = np.array([background_dice, dice_coefficient])

            analysis.testing_class_suited_for_calculation = convert_np_array_to_binary(
                testing_class_suited_for_calculation
            )
            analysis.testing_pixels_per_class = convert_np_array_to_binary(
                testing_pixels_per_class
            )
            analysis.testing_accuracy_per_class = convert_np_array_to_binary(
                testing_accuracy_per_class
            )
            analysis.testing_true_positives_ratio_per_class = convert_np_array_to_binary(
                testing_true_positives_ratio_per_class
            )
            analysis.testing_true_negatives_ratio_per_class = convert_np_array_to_binary(
                testing_true_negatives_ratio_per_class
            )
            analysis.testing_false_positives_ratio_per_class = convert_np_array_to_binary(
                testing_false_positives_ratio_per_class
            )
            analysis.testing_false_negatives_ratio_per_class = convert_np_array_to_binary(
                testing_false_negatives_ratio_per_class
            )
            analysis.testing_precision_per_class = convert_np_array_to_binary(
                testing_precision_per_class
            )
            analysis.testing_mean_iou_per_class = convert_np_array_to_binary(
                testing_mean_iou_per_class
            )
            analysis.testing_dice_coefficient_per_class = convert_np_array_to_binary(
                testing_dice_coefficient_per_class
            )
            analysis.save()


def backwards(apps, schema_editor):
    analysis_model_class = apps.get_model('pytorch_segmentation', 'ModelAnalysis')
    analysis_qs = analysis_model_class.objects.filter(classes=2)
    for analysis in analysis_qs:
        if analysis.classes == 2 and analysis.testing_class_suited_for_calculation:
            class_suited_for_calculation = convert_binary_to_np_array(analysis.testing_class_suited_for_calculation)
            if class_suited_for_calculation[0] == 0:
                continue
            true_positives_ratio_per_class = convert_binary_to_np_array(analysis.testing_true_positives_ratio_per_class)
            true_negatives_ratio_per_class = convert_binary_to_np_array(analysis.testing_true_negatives_ratio_per_class)
            false_positives_ratio_per_class = convert_binary_to_np_array(
                analysis.testing_false_positives_ratio_per_class
            )
            false_negatives_ratio_per_class = convert_binary_to_np_array(
                analysis.testing_false_negatives_ratio_per_class
            )

            analysis.testing_true_positives = true_positives_ratio_per_class[1] * analysis.testing_all_pixels
            analysis.testing_true_negatives = true_negatives_ratio_per_class[1] * analysis.testing_all_pixels
            analysis.testing_false_positives = false_positives_ratio_per_class[1] * analysis.testing_all_pixels
            analysis.testing_false_negatives = false_negatives_ratio_per_class[1] * analysis.testing_all_pixels

            analysis.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0014_alter_modelanalysis_testing_all_pixels'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
