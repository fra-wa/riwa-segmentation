from django.db import migrations


def forwards(apps, schema_editor):
    parameters = apps.get_model('pytorch_segmentation', 'TrainingContinuationParameters')
    parameters = parameters.objects.last()
    if parameters:
        parameters.batch_size = None
        parameters.save()


def backwards(apps, schema_editor):
    parameters = apps.get_model('pytorch_segmentation', 'TrainingContinuationParameters')
    parameters = parameters.objects.last()
    if parameters:
        parameters.batch_size = 16
        parameters.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0009_clean_up_old_parameters'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
