from decimal import Decimal

from django.db import migrations

from pytorch_segmentation import constants


def str_to_dict(online_aug_kwargs_string):
    if not (online_aug_kwargs_string.startswith('{') and online_aug_kwargs_string.endswith('}')):
        # why? example: eval(os.system('rm -rf /')) would delete everything from your computer.
        raise ValueError(f'Cannot eval string to dict! Unsafe! String to evaluate: {online_aug_kwargs_string}')
    return eval(online_aug_kwargs_string)


def forwards(apps, schema_editor):
    analysis_model = apps.get_model('pytorch_segmentation', 'ModelAnalysis')

    for analysis in analysis_model.objects.all():
        if analysis.architecture in constants.TWO_D_NETWORKS and analysis.online_aug_kwargs_string:
            online_aug_kwargs = str_to_dict(analysis.online_aug_kwargs_string)
            if not isinstance(online_aug_kwargs, dict):
                raise ValueError('online_aug_kwargs is not a dictionary!')
            if 'grid_shuffle_active' in online_aug_kwargs.keys():
                online_aug_kwargs.pop('grid_shuffle_active')
                online_aug_kwargs['grid_shuffle_prob'] = 0
            analysis.online_aug_kwargs_string = str(online_aug_kwargs)
            analysis.save()


def backwards(apps, schema_editor):
    analysis_model = apps.get_model('pytorch_segmentation', 'ModelAnalysis')

    for analysis in analysis_model.objects.all():
        if analysis.architecture in constants.TWO_D_NETWORKS and analysis.online_aug_kwargs_string:
            online_aug_kwargs = str_to_dict(analysis.online_aug_kwargs_string)
            if not isinstance(online_aug_kwargs, dict):
                raise ValueError('online_aug_kwargs is not a dictionary!')
            if 'grid_shuffle_prob' in online_aug_kwargs.keys():
                online_aug_kwargs.pop('grid_shuffle_prob')
                online_aug_kwargs['grid_shuffle_active'] = False
            analysis.online_aug_kwargs_string = str(online_aug_kwargs)
            analysis.save()


class Migration(migrations.Migration):

    dependencies = [
        ('pytorch_segmentation', '0021_grid_shuffle'),
    ]

    operations = [
        migrations.RunPython(forwards, backwards),
    ]
