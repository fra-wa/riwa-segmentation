"""
Implemented by: Franz Wagner
Mail: franz.wagner@tu-dresden.de
"""
import torch

from torch.nn import functional

from pytorch_segmentation.dl_models.base_model import BaseModel
from ..blocks.decoders import CRCRDecoderBlock
from ..blocks.stateless_encoders import StatelessEncoderGRU
from ..blocks.stateless_encoders import StatelessEncoderLSTM
from ..blocks.stateless_encoders import StatelessRUNetCenter
from ..blocks.conv_relu_blocks import ConvReluBlock


class SLUNet(BaseModel):
    """
    Segments the current frame of a video or slice of a volume based on previous frames or slices.
    """

    def __init__(self,
                 image_channels,
                 num_classes,
                 rnn_sequence_size,
                 dropout=0.2,
                 use_gru=True):
        """
        Args:
            image_channels: 1 or 3
            num_classes: also called number of labels to segment, 2 for binary, more for multiclass
            rnn_sequence_size: Previous count of images to predict the current one.
                Example: You want to predict slice 11 of a volume and step_size is 5,
                then you pass slice 7, 8, 9, 10, 11 to the model (shape: [B, 5, C, H, W]).
            dropout: last layer dropout to prevent overfitting
            use_gru: if False, LSTM will be used
        """
        super(SLUNet, self).__init__(image_channels, num_classes)
        raise RuntimeError('Currently not supported! Stateful is available.')

        if image_channels != 1 and image_channels != 3:
            raise ValueError('image channels must be 1 or 3 for gray or RGB')

        if not isinstance(num_classes, int) or num_classes < 2:
            raise ValueError('num_classes must be 2 or larger integer')
        if num_classes == 2:
            num_classes = 1

        self.dropout = dropout
        self.rnn_sequence_size = rnn_sequence_size

        encoder = StatelessEncoderGRU if use_gru else StatelessEncoderLSTM

        self.start = ConvReluBlock(in_channels=image_channels, out_channels=32)

        # in channels, hidden channels for gru or lstm cell, out channels
        self.enc1 = encoder(32, [32, 32], 64)  # skip dim = 2 * hidden_out cause cat
        self.enc2 = encoder(64, [64, 64], 128)  # skip: 128
        self.enc3 = encoder(128, [128, 128], 256)  # skip: 256
        self.enc4 = encoder(256, [256, 256], 512)  # skip: 512

        self.center = StatelessRUNetCenter(512, [512, 512])   # center cat skip 4 = 1024

        self.dec4 = CRCRDecoderBlock(1024, 512)
        self.dec3 = CRCRDecoderBlock(512, 256)
        self.dec2 = CRCRDecoderBlock(256, 128)
        self.dec1 = CRCRDecoderBlock(128, 64)

        self.final = torch.nn.Conv2d(64, num_classes, kernel_size=1, padding=0, stride=1)

    def forward(self, input_batch):
        """

        Args:
            input_batch: Tensor shaped like (b, c, h, w)
                b ... batch size (must be larger or equal to rnn_sequence_size)
                c ... channels (1 or 3 --> Gray or RGB)
                h ... height of the frames
                w ... width of the frames

        Examples:
            You have a volume/video of size: 10x15x15  --> [Frames, Height, Width]
            You pass a batch like: [10, C, 15, 15]  .. C = Channels (1 or 3 for gray or rgb)

            example rnn_sequence_size = 4 --> then:
            Model will not predict slices/frames with idx 0, 1, 2, since these are needed for recurrent cell to remember
            Model will predict slices/frames with idx 3, 4, ... 8, 9 --> 9 is index so it's the 10'th slice

            If you need the first slices as well, revert your volume.

        Returns: b x n x h x w segmentation mask
            b ... batch of output images representing last input image of each input batch sequence
            n ... number of classes
            h ... height of input frames
            w ... width of input frames

        """
        if input_batch.shape[0] < self.rnn_sequence_size:
            raise ValueError(f'Input batch must at least contain {self.rnn_sequence_size} images.')

        # looping is less ram expensive than conversion to one tensor: [B F C H W], F ... frames of sequence
        outputs = []
        for batch_number in range(self.rnn_sequence_size, input_batch.shape[0]):
            sequence = input_batch[batch_number-self.rnn_sequence_size:batch_number]
            x = self.start(sequence)  # pass [F, C, H, W] to get to 32 channels

            x, skip1 = self.enc1(x)
            x, skip2 = self.enc2(x)
            x, skip3 = self.enc3(x)
            x, skip4 = self.enc4(x)

            x = self.center(x)  # [1, C, H, W]

            x = self.dec4(x, skip4)
            x = self.dec3(x, skip3)
            x = self.dec2(x, skip2)
            x = self.dec1(x, skip1)

            x = self.final(functional.dropout2d(x, p=self.dropout))

            outputs.append(x)

        output_batch = torch.stack(outputs).squeeze(dim=1)

        return output_batch
