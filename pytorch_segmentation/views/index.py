from django.shortcuts import render


def index(request):
    context = {
        'navbar_view': 'index',
    }
    return render(request, 'pytorch_segmentation/index.html', context)
