from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.shortcuts import render, redirect

from pytorch_segmentation.forms import TrainingDatasetForm
from pytorch_segmentation.models import TrainingDataset, ModelAnalysis


def edit_dataset(request, dataset_pk):
    try:
        dataset = TrainingDataset.objects.get(pk=dataset_pk)
    except ObjectDoesNotExist:
        dataset = None

    form = TrainingDatasetForm(instance=dataset)

    if request.method == 'POST':
        form = TrainingDatasetForm(request.POST, instance=dataset)

        if form.is_valid():
            if form.has_changed():
                try:
                    form.save()
                    messages.add_message(request, messages.SUCCESS, 'Saved dataset')
                except IntegrityError:
                    # only one of those can be set, other is an empty string
                    name = form.cleaned_data['name'] + form.cleaned_data['folder_choice']
                    existing_dataset = TrainingDataset.objects.get(name=name)

                    messages.add_message(
                        request,
                        messages.ERROR,
                        'The dataset already exists! You were redirected to this dataset.'
                    )
                    return redirect('pytorch_segmentation:edit_dataset', dataset_pk=existing_dataset.pk)

            return redirect('pytorch_segmentation:datasets_overview')

    context = {
        'form': form,
        'dataset': dataset,
        'navbar_view': 'datasets',
    }
    return render(request, 'pytorch_segmentation/dataset/dataset_edit.html', context)


def delete_dataset(request, dataset_pk):
    try:
        dataset = TrainingDataset.objects.get(pk=dataset_pk)
    except ObjectDoesNotExist:
        messages.add_message(request, messages.WARNING, 'The dataset does not exist.')
        return redirect('pytorch_segmentation:datasets_overview')

    analyses = ModelAnalysis.objects.filter(dataset=dataset)
    if analyses:
        messages.add_message(
            request,
            messages.WARNING,
            f'The dataset {dataset.name} can not be deleted. '
            f'There are multiple models ({len(analyses)}) trained on the dataset.'
        )
    else:
        messages.add_message(request, messages.SUCCESS, f'The dataset {dataset.name} was deleted.')
        dataset.delete()

    return redirect('pytorch_segmentation:datasets_overview')


def datasets_overview(request):
    datasets = TrainingDataset.objects.all().order_by('name')

    context = {
        'datasets': datasets,
        'navbar_view': 'datasets',
    }
    return render(request, 'pytorch_segmentation/dataset/datasets.html', context)
