from django.apps import AppConfig


class PytorchSegmentationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pytorch_segmentation'
    verbose_name = "Pytorch Segmentation"
