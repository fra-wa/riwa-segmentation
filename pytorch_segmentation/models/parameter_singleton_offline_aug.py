from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

"""
This is created once to store the users offline image and volume augmentation input over time.
"""


class OfflineImageAugmentationParameters(models.Model):
    images_folder = models.CharField(
        verbose_name='Path to images',
        max_length=256,
        blank=True,
        help_text='Pass the path to the images or provide the dataset folder.',
    )
    masks_folder = models.CharField(
        verbose_name='Path to masks',
        max_length=256,
        blank=True,
        help_text='Pass the path to the masks or provide the dataset folder.',
    )
    dataset_folder = models.CharField(
        verbose_name='Dataset folder',
        max_length=256,
        blank=True,
        help_text='You can also select an existing dataset folder.',
    )
    output_folder = models.CharField(
        verbose_name='Output folder',
        max_length=256,
        blank=True,
        help_text='Pass the path where to store the augmented data. Defaults to the input folders.',
    )
    min_dimension = models.PositiveIntegerField(
        verbose_name='Minimum image dimension after augmentation',
        default=256,
        help_text='0 for no min limit, else every calculated img with height or width smaller than this is not saved.',
    )
    # exponential moving average = ema
    max_augmentations = models.PositiveIntegerField(
        verbose_name='Maximum augmentations per image',
        default=15,
        help_text='Pass an upper boundary of augmentations per image. A lower number results in more output images '
                  'having only geometric manipulations. Geometric manipulations are preserved in most pixel '
                  'manipulated images anyway.',
        validators=[MinValueValidator(1)],
    )
    total_augmentations = models.PositiveIntegerField(
        verbose_name='Total stored images',
        default=10000,
        validators=[MinValueValidator(0)],
        help_text='How many samples should be stored in total. Initially you will get: '
                  '(Max augmentations per image) * (images samples)! Depending on the dataset size, this may be way '
                  'too much, therefore you can limit it here. 0 to keep all.',
    )
    max_rot_angle = models.PositiveSmallIntegerField(
        verbose_name='Maximum rotation angle',
        default=30,
        help_text='Maximum angle, the image gets rotated (&#177; this angle) (Min: 5, Max: 175). '
                  'Actual angle will be in between 5 and this angle.',
        validators=[MinValueValidator(5), MaxValueValidator(175)],
    )

    # geometric:
    do_rotate = models.BooleanField(
        verbose_name='Rotating',
        default=True,
    )
    do_resize = models.BooleanField(
        verbose_name='Resizing',
        default=True,
    )
    do_tilt_backwards = models.BooleanField(
        verbose_name='Backward tilting',
        default=True,
    )
    do_tilt_forward = models.BooleanField(
        verbose_name='Forward tilting',
        default=True,
    )
    do_tilt_left = models.BooleanField(
        verbose_name='Tilting to the left',
        default=True,
    )
    do_tilt_right = models.BooleanField(
        verbose_name='Tilting to the right',
        default=True,
    )
    do_elastic_distortion = models.BooleanField(
        verbose_name='Elastic distortion',
        default=True,
    )
    do_grid_distortion = models.BooleanField(
        verbose_name='Grid distortion',
        default=True,
    )
    do_optical_distortion = models.BooleanField(
        verbose_name='Optical distortion',
        default=True,
    )
    do_random_crop = models.BooleanField(
        verbose_name='Random cropping',
        default=True,
    )
    do_random_grid_shuffle = models.BooleanField(
        verbose_name='Grid shuffle',
        default=False,
    )
    do_squeeze_height = models.BooleanField(
        verbose_name='Height squeezing',
        default=True,
    )
    do_squeeze_width = models.BooleanField(
        verbose_name='Width squeezing',
        default=True,
    )
    do_flip_horizontal = models.BooleanField(
        verbose_name='Horizontal flipping',
        default=True,
    )
    do_flip_vertical = models.BooleanField(
        verbose_name='Vertical flipping',
        default=False,
    )

    # color:
    do_to_gray_and_adaptive_histogram = models.BooleanField(
        verbose_name='Adaptive histogram normalization',
        default=False,
        help_text='Better histogram normalization, needs more time. Converts to gray.',
    )
    do_brightness = models.BooleanField(
        verbose_name='Brightness changing',
        default=True,
    )
    do_contrast = models.BooleanField(
        verbose_name='Contrast changing',
        default=True,
    )
    do_gaussian_filter = models.BooleanField(
        verbose_name='Blurring (gaussian filter)',
        default=True,
    )
    do_gaussian_noise = models.BooleanField(
        verbose_name='Noising (gaussian noise)',
        default=True,
    )
    do_to_gray_and_histogram_normalization = models.BooleanField(
        verbose_name='Histogram normalization',
        default=True,
        help_text='Standard normalization, no adaptive changing. Converts to gray.',
    )
    do_to_gray_and_non_local_means_filter = models.BooleanField(
        verbose_name='Non local means filter',
        default=True,
        help_text='Converts to gray.',
    )
    do_sharpen = models.BooleanField(
        verbose_name='Sharpening',
        default=True,
    )
    do_random_erasing = models.BooleanField(
        verbose_name='Random erasing',
        default=True,
    )
    do_channel_shuffle = models.BooleanField(
        verbose_name='Channel shuffling (rgb only)',
        default=True,
    )
    do_color_to_hsv = models.BooleanField(
        verbose_name='Color to hsv conversion (rgb only)',
        default=True,
    )
    do_iso_noise = models.BooleanField(
        verbose_name='Iso noising (rgb only)',
        default=True,
    )
    do_random_fog = models.BooleanField(
        verbose_name='Random fog (rgb only)',
        default=True,
    )
    do_random_rain = models.BooleanField(
        verbose_name='Random rain (rgb only)',
        default=True,
    )
    do_random_shadow = models.BooleanField(
        verbose_name='Random shadows (rgb only)',
        default=True,
    )
    do_random_snow = models.BooleanField(
        verbose_name='Random snow (rgb only)',
        default=True,
    )
    do_random_sun_flair = models.BooleanField(
        verbose_name='Random sun flair (rgb only)',
        default=True,
    )
    do_rgb_shift = models.BooleanField(
        verbose_name='Rgb shifting (rgb only)',
        default=True,
    )
    do_solarize = models.BooleanField(
        verbose_name='Solarizing (rgb only)',
        default=True,
    )
    do_to_gray = models.BooleanField(
        verbose_name='To gray conversion (rgb only)',
        default=True,
    )
    do_to_sepia = models.BooleanField(
        verbose_name='To sepia conversion (rgb only)',
        default=True,
    )


class OfflineVolumeAugmentationParameters(models.Model):
    images_folder = models.CharField(
        verbose_name='Path to images',
        max_length=256,
        blank=True,
        help_text='Pass the path to the folder containing volumes or provide the dataset folder.',
    )
    masks_folder = models.CharField(
        verbose_name='Path to masks',
        max_length=256,
        blank=True,
        help_text='Pass the path to the folder containing ground truth volumes or provide the dataset folder.',
    )
    dataset_folder = models.CharField(
        verbose_name='Path to dataset folder',
        max_length=256,
        blank=True,
        help_text='You can also select an existing dataset folder.',
    )
    output_folder = models.CharField(
        verbose_name='Output folder',
        max_length=256,
        help_text='Pass the path where to store the augmented data. Defaults to the input folders.',
    )
    min_dimension_x = models.PositiveIntegerField(
        verbose_name='Minimum x dimension after augmentation',
        default=200,
        help_text='minimum width of the resulting augmented volumes',
    )
    min_dimension_y = models.PositiveIntegerField(
        verbose_name='Minimum y dimension after augmentation',
        default=200,
        help_text='minimum height of the resulting augmented volumes',
    )
    min_dimension_z = models.PositiveIntegerField(
        verbose_name='Minimum z dimension after augmentation',
        default=20,
        help_text='minimum depth of the resulting augmented volumes',
    )
    rotation_angle = models.PositiveSmallIntegerField(
        verbose_name='Rotation angle iteration',
        default=30,
        validators=[MinValueValidator(1), MaxValueValidator(359)],
        help_text='Rotation: n times by rotation_angle degrees until full 360° (pitch yaw roll).',
    )
    # function_names_as_folder deprecated
    max_augmentations = models.PositiveIntegerField(
        verbose_name='Maximum augmentations per volume',
        default=30,
        help_text='Pass 0 to create all possible combinations. Set a reasonable value for "Total stored volumes", as '
                  'this could create too many samples!',
    )
    total_augmentations = models.PositiveIntegerField(
        verbose_name='Total stored volumes',
        default=100,
        validators=[MinValueValidator(0)],
        help_text='How many augmented volumes should be stored in total. Initially you will get: '
                  '(Max augmentations per volume) * (volume samples)! Depending on the dataset size, this may be way '
                  'too much, therefore you can limit it here. 0 to keep all.',
    )

    do_rotate_x = models.BooleanField(
        verbose_name='Rotate around x',
        default=True,
    )
    do_rotate_y = models.BooleanField(
        verbose_name='Rotate around y',
        default=True,
    )
    do_rotate_z = models.BooleanField(
        verbose_name='Rotate around z',
        default=True,
    )
    do_resize = models.BooleanField(
        verbose_name='Resize',
        default=True,
    )
    do_random_flip = models.BooleanField(
        verbose_name='Random flip',
        default=True,
    )
    do_random_tilt = models.BooleanField(
        verbose_name='Random tilt',
        default=True,
    )
    do_random_squeeze = models.BooleanField(
        verbose_name='Random squeeze',
        default=True,
    )
    do_random_crop = models.BooleanField(
        verbose_name='Random crop',
        default=True,
    )
    do_noise = models.BooleanField(
        verbose_name='Noise adding',
        default=True,
    )
    do_blur = models.BooleanField(
        verbose_name='Blurring',
        default=True,
    )
    do_sharpen = models.BooleanField(
        verbose_name='Sharpen',
        default=True,
    )
    do_contrast = models.BooleanField(
        verbose_name='Contrast manipulation',
        default=True,
    )
    do_brightness = models.BooleanField(
        verbose_name='Brightness manipulation',
        default=True,
    )
    do_random_shadow = models.BooleanField(
        verbose_name='Random shadow',
        default=False,
    )
    do_random_erasing = models.BooleanField(
        verbose_name='Random erasing',
        default=False,
    )
