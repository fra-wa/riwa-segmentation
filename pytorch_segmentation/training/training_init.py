import datetime
import logging
import numpy as np
import os
import psutil
import time
import torch

from django.core.exceptions import ObjectDoesNotExist

from .data_loading import set_up_loaders, get_dataset_mean_and_std
from .data_sets import Dataset3D, Dataset2D
from .profiler import TrainingProfiler
from .saving import get_trained_model_folder
from .saving import save_history
from .saving import save_model
from .testing import run_test_loop
from .training_execution import train_loop, valid_loop

from .. import constants
from ..data_augmentation.online_augmentation_pipelines import ImageAugmentationPipeline
from ..data_augmentation.online_augmentation_pipelines import VolumeAugmentationPipeline
from ..dl_models.loading import get_model
from ..dl_models.loading import load_checkpoint_to_continue_training
from ..file_handling.utils import get_file_paths_in_folder
from ..models import LogFile, OnlineImageAugParameters, OnlineVolumeAugParameters
from ..models import ModelAnalysis
from ..models import TrainingDataset
from ..train_data_preprocessing.auto_weighting import get_weights
from ..train_data_preprocessing.dataset_loading import prepare_dataset
from ..temporary_parameters.parameters import FixedParameters
from ..temporary_parameters.user_information import show_best_five_stats
from ..temporary_parameters.user_information import show_stats
from ..utils import show_memory, set_up_reproducibility
from ..utils import timezone_datetime


def set_up_parameters(parameters):

    try:
        params = parameters.get_fixed_parameters()
    except AttributeError:
        # already got fixed parameters
        params = parameters

    if not isinstance(params, FixedParameters):
        raise RuntimeError('Expecting fixed parameters!')

    if params.device == 'cuda':
        params.device = f'cuda:0'

    if params.save_every == 0:
        params.save_every = params.epochs

    params.show_parameters()

    logging.info('Class with lowest value is treated as background.')

    if params.reproduce:
        set_up_reproducibility()

    if params.show_ram:
        logging.info(f'Initial ram usage of device: {params.device}')
        show_memory(params.device)

    return params


def get_loss(params):
    weights = params.weight_per_class
    if weights is None:
        raise ValueError('No weights passed to loss!')
    if not isinstance(weights, np.ndarray):
        if isinstance(weights, list):
            weights = np.array(weights)
        else:
            raise ValueError('params.weight_per_class must be a list or np.ndarray!')
    weights = torch.from_numpy(weights)

    if params.classes == 2:
        # pos_weight: weighting the positives --> weight of ones, weight of zeros = 1
        if not params.auto_weight:
            weights = [None, None]
        loss = torch.nn.BCEWithLogitsLoss(pos_weight=weights[1]).to(params.device)
    else:
        if not params.auto_weight:
            weights = None
        loss = torch.nn.CrossEntropyLoss(weight=weights.float(), reduction='mean').to(params.device)
    return loss


def get_log_object():
    process = psutil.Process(os.getpid())
    log_files = LogFile.objects.filter(
        process_start_datetime=timezone_datetime(datetime.datetime.fromtimestamp(round(process.create_time()))),
        process_id=os.getpid(),
        execution_process=constants.DL_EXECUTION_TRAIN,
    )
    if log_files:
        if log_files.count() > 1:
            logging.info('Found multiple log files, live view disabled.')
            return None
        else:
            return log_files[0]
    return None


def set_up_db_models(params, training_started_time, training_loader, validation_loader, online_aug_kwargs=None):
    if not isinstance(training_started_time, datetime.datetime):
        raise ValueError('training_started_time needs to be a tz aware datetime object')
    if training_started_time.tzinfo is None:
        raise ValueError('training_started_time needs to be a tz aware datetime object')

    try:
        dataset = TrainingDataset.objects.get(folder_name_on_device=params.dataset_name)
        if dataset.number_training_images != len(training_loader.dataset):
            dataset.number_training_images = len(training_loader.dataset)
            dataset.save()
        elif dataset.number_validation_images != len(validation_loader.dataset):
            dataset.number_validation_images = len(validation_loader.dataset)
            dataset.save()
        logging.info('Found database training dataset instance.')
    except (ObjectDoesNotExist, ValueError):
        dataset = TrainingDataset.objects.create(
            name=params.dataset_name,
            folder_name_on_device=params.dataset_name,
            number_training_images=len(training_loader.dataset),
            number_validation_images=len(validation_loader.dataset),
        )
        logging.info('Created new database training dataset instance.')

    try:
        model_analysis = ModelAnalysis.objects.filter(
            architecture=params.architecture,
            backbone=params.backbone,
            channels=params.channels,
            classes=params.classes,
            input_height=params.input_size,
            input_width=params.input_size,
            input_depth=params.depth_3d,
            online_aug_kwargs_string=str(online_aug_kwargs),
            created_datetime=training_started_time,  # this should be enough to filter correctly locally
        )[0]
        if not model_analysis.normalization:
            model_analysis.normalization = params.norm
            model_analysis.save()
        logging.info('Found related analysis entry, continuing.')
    except IndexError:
        model_analysis = ModelAnalysis.objects.create(
            created_datetime=training_started_time,
            architecture=params.architecture,
            backbone=params.backbone,
            dataset=dataset,
            total_epochs=0,
            batch_size=params.batch_size,
            channels=params.channels,
            classes=params.classes,
            input_height=params.input_size,
            input_width=params.input_size,
            input_depth=params.depth_3d if params.architecture in constants.THREE_D_NETWORKS else None,
            online_aug_kwargs_string=str(online_aug_kwargs),
            pretrained=params.pretrained if params.backbone else None,
            normalization=constants.NORMALIZATION_DICT[params.norm],
        )
        logging.info('Created analysis database entry.')

    log_file = get_log_object()
    if log_file is not None:
        log_file.model_analysis = model_analysis
        log_file.save()
    else:
        logging.info('Could not find the related log file object. No live view available.')

    return model_analysis


def get_best_stored_checkpoint(model_dir, history, model_analysis):
    """
    Depending on the valid loss and stored checkpoints

    Args:
        model_dir:
        history:
        model_analysis:

    Returns: checkpoint, related_data
    """
    checkpoints = get_file_paths_in_folder(model_dir, extension='.pt')
    if not checkpoints:
        return None
    # named like: checkpoint_epoch_xyz.pt --> split at _, select xyz.pt, split, profit
    checkpoint_epochs = [int(os.path.basename(checkpoint).split('_')[-1].split('.')[0]) for checkpoint in checkpoints]

    checkpoint_indexes = [epoch - 1 for epoch in checkpoint_epochs]
    reduced_history = [history[index] for index in checkpoint_indexes]
    best_stored_epoch_loss = sorted(reduced_history, key=lambda x: x[2])[0]
    best_actual_epoch_loss = sorted(history, key=lambda x: x[2])[0]

    best_stored_epoch_acc = sorted(reduced_history, key=lambda x: x[4], reverse=True)[0]
    best_actual_epoch_acc = sorted(history, key=lambda x: x[4], reverse=True)[0]

    model_analysis.best_epoch = best_actual_epoch_loss[0]
    model_analysis.save()

    logging.info(f'Best epoch (loss): {best_actual_epoch_loss[0]}')
    logging.info(f'Best epoch (accuracy): {best_actual_epoch_acc[0]}')
    if best_stored_epoch_loss[0] != best_actual_epoch_loss[0]:
        logging.info(f'Best saved epoch (loss): {best_stored_epoch_loss[0]}')
    if best_stored_epoch_acc[0] != best_actual_epoch_acc[0]:
        logging.info(f'Best saved epoch (accuracy): {best_stored_epoch_acc[0]}')
    logging.info(f'Values of best epoch (loss): {best_stored_epoch_loss[0]}')
    logging.info(
        f'train_loss: {best_stored_epoch_loss[1]:.6f}, valid_loss: {best_stored_epoch_loss[2]:.4f}, '
        f'train_acc: {best_stored_epoch_loss[3]:.6f}, valid_acc {best_stored_epoch_loss[4]:.4f}'
    )
    logging.info(f'This checkpoint (loss) should be treated as the best overall model of your training!')
    epochs = history[-1][0]
    file_number = str(best_stored_epoch_loss[0]).zfill(len(str(epochs)))
    file_name = f'checkpoint_epoch_{file_number}.pt'
    checkpoint_path = os.path.join(model_dir, file_name)
    if not os.path.isfile(checkpoint_path):
        raise RuntimeError('Mistakes were made')

    return checkpoint_path, best_stored_epoch_loss


def create_online_aug_kwargs_from_db(architecture):
    if architecture in constants.TWO_D_NETWORKS:
        if OnlineImageAugParameters.objects.count() == 0:
            OnlineImageAugParameters.objects.create()
        online_aug_parameters = OnlineImageAugParameters.objects.last()
    else:
        if OnlineVolumeAugParameters.objects.count() == 0:
            OnlineVolumeAugParameters.objects.create()
        online_aug_parameters = OnlineVolumeAugParameters.objects.last()

    fields = online_aug_parameters._meta.fields
    online_aug_kwargs = {}
    for field in fields:
        if field.attname == 'id':
            continue
        online_aug_kwargs[field.attname] = online_aug_parameters.__getattribute__(field.attname)

    return online_aug_kwargs


def setup_online_aug_pipe(online_aug_kwargs, architecture, input_size, input_depth=None, log_info=True):
    global_strength = online_aug_kwargs['global_strength'] / 100
    max_augmentations = online_aug_kwargs['max_augmentations']
    max_rot_angle = online_aug_kwargs['max_rot_angle']
    resizing_scale = (
        float(online_aug_kwargs['resizing_scale_lower']),
        float(online_aug_kwargs['resizing_scale_upper'])
    )
    squeeze_scale = (
        float(online_aug_kwargs['squeeze_scale_lower']),
        float(online_aug_kwargs['squeeze_scale_upper'])
    )
    tilting_start_end_factor = (
        float(online_aug_kwargs['tilting_start_factor']),
        float(online_aug_kwargs['tilting_end_factor'])
    )

    if architecture in constants.TWO_D_NETWORKS:
        augmentation_pipeline = ImageAugmentationPipeline(
            policy=online_aug_kwargs['policy'],
            expected_input_size=input_size,
            global_strength=global_strength,
            max_augmentations=max_augmentations,
            max_rot_angle=max_rot_angle,
            resizing_scale=resizing_scale,
            squeeze_scale=squeeze_scale,
            tilting_start_end_factor=tilting_start_end_factor,

            brightness_prob=online_aug_kwargs['brightness_prob'] / 100,
            contrast_prob=online_aug_kwargs['contrast_prob'] / 100,
            gaussian_filter_prob=online_aug_kwargs['gaussian_filter_prob'] / 100,
            gaussian_noise_prob=online_aug_kwargs['gaussian_noise_prob'] / 100,
            random_erasing_prob=online_aug_kwargs['random_erasing_prob'] / 100,
            channel_shuffle_prob=online_aug_kwargs['channel_shuffle_prob'] / 100,
            color_to_hsv_prob=online_aug_kwargs['color_to_hsv_prob'] / 100,
            iso_noise_prob=online_aug_kwargs['iso_noise_prob'] / 100,
            random_fog_prob=online_aug_kwargs['random_fog_prob'] / 100,
            random_rain_prob=online_aug_kwargs['random_rain_prob'] / 100,
            random_shadow_prob=online_aug_kwargs['random_shadow_prob'] / 100,
            random_snow_prob=online_aug_kwargs['random_snow_prob'] / 100,
            random_sun_flair_prob=online_aug_kwargs['random_sun_flair_prob'] / 100,
            rgb_shift_prob=online_aug_kwargs['rgb_shift_prob'] / 100,
            solarize_prob=online_aug_kwargs['solarize_prob'] / 100,
            to_gray_prob=online_aug_kwargs['to_gray_prob'] / 100,
            to_sepia_prob=online_aug_kwargs['to_sepia_prob'] / 100,

            elastic_distortion_prob=online_aug_kwargs['elastic_distortion_prob'] / 100,
            flip_prob=online_aug_kwargs['flip_prob'] / 100,
            grid_distortion_prob=online_aug_kwargs['grid_distortion_prob'] / 100,
            optical_distortion_prob=online_aug_kwargs['optical_distortion_prob'] / 100,
            random_crop_prob=online_aug_kwargs['random_crop_prob'] / 100,
            resize_prob=online_aug_kwargs['resize_prob'] / 100,
            rotate_by_angle_prob=online_aug_kwargs['rotate_by_angle_prob'] / 100,
            tilt_prob=online_aug_kwargs['tilt_prob'] / 100,
            squeeze_prob=online_aug_kwargs['squeeze_prob'] / 100,
            grid_shuffle_prob=online_aug_kwargs['grid_shuffle_prob'] / 100,
        )
    else:
        if input_depth is None:
            raise ValueError('Input depth cannot be None!')
        augmentation_pipeline = VolumeAugmentationPipeline(
            policy=online_aug_kwargs['policy'],
            expected_input_size=input_size,
            expected_input_depth=input_depth,
            global_strength=global_strength,
            max_augmentations=max_augmentations,
            max_rot_angle=max_rot_angle,
            resizing_scale=resizing_scale,
            squeeze_scale=squeeze_scale,
            tilting_start_end_factor=tilting_start_end_factor,
            depth_flip_allowed=online_aug_kwargs['depth_flip_allowed'],

            add_blur_3d_prob=online_aug_kwargs['add_blur_3d_prob'] / 100,
            add_noise_3d_prob=online_aug_kwargs['add_noise_3d_prob'] / 100,
            brightness_3d_prob=online_aug_kwargs['brightness_3d_prob'] / 100,
            contrast_3d_prob=online_aug_kwargs['contrast_3d_prob'] / 100,
            random_erasing_3d_prob=online_aug_kwargs['random_erasing_3d_prob'] / 100,
            sharpen_3d_with_blur_prob=online_aug_kwargs['sharpen_3d_with_blur_prob'] / 100,
            random_shadow_3d_prob=online_aug_kwargs['random_shadow_3d_prob'] / 100,

            flip_prob=online_aug_kwargs['flip_prob'] / 100,
            random_crop_3d_prob=online_aug_kwargs['random_crop_3d_prob'] / 100,
            random_resize_3d_prob=online_aug_kwargs['random_resize_3d_prob'] / 100,
            rotate_z_3d_prob=online_aug_kwargs['rotate_z_3d_prob'] / 100,
            squeeze_prob=online_aug_kwargs['squeeze_prob'] / 100,
            tilt_prob=online_aug_kwargs['tilt_prob'] / 100,
        )

        if log_info:
            logging.info('-------------------------')
            logging.info('Volume Augmentation is active!')
            logging.info(
                'Attention: to get the best results, '
                'ROTATE BEFORE: rotate initial data by 90, 180 and 270 degrees around x and y'
            )
            logging.info('You can use the Volume Augmentation Tool and skip all other augmentations.')
            logging.info('Explanation:')
            logging.info('The online augmentation only rotates around z since the depth of a network might be totally '
                         'different from height and width.')
            logging.info('h and w are most likely a constraint by a networks architecture')
            logging.info('e.g. UNet3D--> h and w must be divisible')
            logging.info("by two**n (n = depth of UNet3D -> 64/(2**4) for depth = 4)) and the depth isn't.")
            logging.info('The depth might be 16, 20, 32, 40, ... and is still valid.')
            logging.info('-------------------------')

    return augmentation_pipeline


def start_training(parameters,
                   continue_training=False,
                   fixed_end_epoch=False,
                   logger=None,
                   execute_test=True,
                   ):
    """

    Args:
        parameters: Parameters instance (user interaction)
        continue_training:
        fixed_end_epoch: For continuation. If True, the params.epochs will be the maximum epoch and not added to the
            latest epoch. E.g. if params.epochs = 100 and latest checkpoint loaded was from epoch 98, then only 2
            further epochs will be executed.
        logger: needed for multiprocessing
        execute_test: whether to execute the automatic test at the end of a training

    Returns:

    """

    params = set_up_parameters(parameters)
    if not params.online_aug:
        params.aug_strength = 0

    if continue_training:
        user_defined_batch_size = params.batch_size
        params, model, optimizer_state, history_dict, online_aug_kwargs, training_started_time = \
            load_checkpoint_to_continue_training(params)
        if user_defined_batch_size:
            if params.batch_size is not None:
                if params.batch_size != user_defined_batch_size:
                    logging.info(
                        f'WARNING: Checkpoint was trained with a batch size of: {params.batch_size} '
                        f'but you defined: {user_defined_batch_size}!'
                    )
                    logging.info(f'WARNING: Continuing with new batch size: {user_defined_batch_size}!')
            params.batch_size = user_defined_batch_size
        optimizer = torch.optim.Adam(model.parameters())
        optimizer.load_state_dict(optimizer_state)

        # crucial!!
        # issue: using more VRam when loading from checkpoint than starting from scratch:
        # since loading is in external func, there is no problem but the state dict is heavy!
        # https://discuss.pytorch.org/t/gpu-memory-usage-increases-by-90-after-torch-load/9213/9
        # 0/11441 MiB  --> initial
        # 6270/11441 MiB  --> loaded checkpoint
        # 6270/11441 MiB  --> set up optimizer
        # 2060/11441 MiB  --> deleted optimizer_state dict
        del optimizer_state
        torch.cuda.empty_cache()

        start_epoch = history_dict['epochs'][-1]
        if not fixed_end_epoch:
            params.epochs = params.epochs + start_epoch

    else:
        online_aug_kwargs = create_online_aug_kwargs_from_db(params.architecture)

        params.aug_strength = online_aug_kwargs['global_strength']
        params.aug_start_epoch = online_aug_kwargs['online_aug_start_epoch']
        model = get_model(model_name=params.architecture, channels=params.channels, classes=params.classes,
                          device=params.device, backbone=params.backbone, pretrained=params.pretrained,
                          use_gru=params.use_gru, normalization=params.norm, in_size=params.input_size,
                          log_information=True)
        if params.lr is not None:
            optimizer = torch.optim.Adam(model.parameters(), lr=params.lr)
        else:
            optimizer = torch.optim.Adam(model.parameters())
        start_epoch = 0
        history_dict = {
            'epochs': [],  # epoch of history will start at 1
            'train_loss': [],
            'valid_loss': [],
            'train_acc': [],
            'valid_acc': [],
            'aug_strength': [],
        }
        process = psutil.Process(os.getpid())
        process_started_timestamp = round(process.create_time())
        training_started_time = timezone_datetime(datetime.datetime.fromtimestamp(process_started_timestamp))

    if start_epoch == params.epochs:
        logging.info(f'Already trained the model for {start_epoch} epochs. Stopping.')
        return

    if (params.norm == 'bn' and params.batch_size < 16) or (params.norm == 'gn' and params.batch_size >= 16):
        logging.warning(
            f'\nWarning: Using a batch size of {params.batch_size} and normalization {params.norm}.\n'
            f'Recommended usage: batch size >= 16: use BatchNorm (bn), batch size < 16: use GroupNorm (gn)'
        )

    if params.online_aug:
        logging.info('#####################################################')
        logging.info('Using the following online aug parameters:')
        for key, value in online_aug_kwargs.items():
            logging.info(f'Parameter: {key}: {value}')
        logging.info('#####################################################')

    logging.info(
        f'The model has {round(model.get_parameter_count()/1000000, 1)} mio parameters ({model.get_parameter_count()}).'
    )

    image_paths, mask_paths, valid_images_paths, valid_masks_paths = prepare_dataset(params, logger=logger)

    # double check here in case someone deleted the std and mean file --> we do not recalculate everything
    if model.dataset_mean is not None and model.dataset_std is not None:
        dataset_mean, dataset_std = model.dataset_mean, model.dataset_std
    else:
        dataset_mean, dataset_std = get_dataset_mean_and_std(params, logger=logger)

    if params.auto_weight:
        dataset_folder = os.path.join(constants.DATASET_FOLDER, params.dataset_name)
        params.weight_per_class = get_weights(
            dataset_folder, mask_paths, params.classes, params.input_size, logger=logger
        )
    else:
        params.weight_per_class = np.ones(params.classes)

    loss_func = get_loss(params)

    training_loader, validation_loader = set_up_loaders(
        image_paths,
        mask_paths,
        params,
        valid_images_paths=valid_images_paths,
        valid_masks_paths=valid_masks_paths,
        dataset_mean=dataset_mean,
        dataset_std=dataset_std,
    )

    if not isinstance(training_loader.dataset, (Dataset2D, Dataset3D)):
        raise ValueError('Error! Wrong Dataloader.Dataset found. Dataset must be Dataset2D or Dataset3D!')

    model_folder = get_trained_model_folder(params, continue_training, custom_folder_name=None)

    epochs = params.epochs

    if params.show_ram:
        logging.info(f'Ram usage after model and parameter loading of device: {params.device}')
        show_memory(params.device)

    profiler = TrainingProfiler(
        history_dict=history_dict,
        aug_strength=params.aug_strength if params.online_aug else 0,
        aug_start_epoch=params.aug_start_epoch if params.online_aug else 0,
    )
    history, header = profiler.get_history()

    if params.online_aug:
        augmentation_pipeline = setup_online_aug_pipe(
            online_aug_kwargs, params.architecture, params.input_size, params.depth_3d
        )
    else:
        augmentation_pipeline = None
        online_aug_kwargs = {}

    training_loader.dataset.aug_pipeline = augmentation_pipeline
    if start_epoch >= params.aug_start_epoch:
        training_loader.dataset.online_aug_active = True

    model_analysis = set_up_db_models(
        params, training_started_time, training_loader, validation_loader, online_aug_kwargs
    )

    header = None
    epoch = 0
    best_loss = 10
    best_val_acc = 0
    start_time = time.time()
    analysis_10_epochs_start_time = 0

    for epoch in range(start_epoch, epochs):
        logging.info(f'Epoch: {epoch + 1}/{epochs}')
        if epoch == params.aug_start_epoch:
            training_loader.dataset.online_aug_active = True
        if epoch - start_epoch == 2:
            analysis_10_epochs_start_time = time.time()
        if epoch - start_epoch == 12:
            model_analysis.training_duration_10_epochs = time.time() - analysis_10_epochs_start_time
            model_analysis.save()
        train_loss, train_acc, model = train_loop(params, model, training_loader, loss_func, optimizer, epoch, epochs)
        valid_loss, valid_acc, model = valid_loop(params, model, validation_loader, loss_func, epoch, epochs)

        profiler.update(epoch + 1, train_loss, valid_loss, 100 * train_acc, 100 * valid_acc)

        history, header = profiler.get_history()
        model_analysis.total_epochs = epoch + 1
        model_analysis.save()
        model_analysis.save_history(history, header)

        if (epoch + 1) % params.save_every == 0 or epoch + 1 == epochs or valid_loss < best_loss or \
                valid_acc > best_val_acc:
            if valid_loss < best_loss:
                logging.info('New best model (validation loss).')
                best_loss = valid_loss

            if valid_acc > best_val_acc:
                logging.info('New best model (validation accuracy).')
                best_val_acc = valid_acc

            save_model(
                params,
                history,
                model,
                optimizer,
                model_folder,
                dataset_mean,
                dataset_std,
                online_aug_kwargs,
                model_analysis,
            )

        show_stats(history, epochs, start_epoch, start_time)

    # store real used time, not just approximation.
    model_analysis.training_duration_10_epochs = (time.time() - start_time) / (epochs - start_epoch) * 10
    model_analysis.save()
    model_analysis.save_history(history, header)

    if epoch >= 1:
        save_history(history, model_folder, header=header)

    show_best_five_stats(history)
    best_saved_checkpoint, best_saved_epoch_data = get_best_stored_checkpoint(model_folder, history, model_analysis)
    model_analysis.refresh_from_db()

    testing_dir = os.path.join(constants.DATASET_FOLDER, params.dataset_name, 'test')
    if os.path.isdir(testing_dir) and best_saved_checkpoint is not None and execute_test:
        storage = run_test_loop(params, best_saved_checkpoint, testing_dir, logger=logger)
        if storage is not None:
            model_analysis.save_test_storage(storage, best_saved_epoch_data[0])

    model_analysis.save()
