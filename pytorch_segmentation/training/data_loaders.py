import cv2
import logging
import os
import random
import torch

from .transformations import get_gray_image_transformation
from .transformations import get_rgb_image_transformation


class FakeDataset:
    """
    Class to ensure pytorch compatibility.

    Training code relies on some pytorch DataLoader implementations like len(DataLoader.dataset)
    """
    def __init__(self, input_batches=None):
        self.length = sum([len(sub_list) for sub_list in input_batches])

    def __len__(self):
        return self.length


class LoaderBase:
    def __init__(self, shuffle=True):

        self.shuffle = shuffle
        self.input_batches = []
        self.target_batches = []

    def load_sequence_inputs_to_tensor(self, image_paths):
        raise NotImplementedError

    def load_batch_tensor(self, item):
        raise NotImplementedError

    @staticmethod
    def drop_last_batch_of_sequence(input_batch_sequence, target_batch_sequence):
        try:
            if len(input_batch_sequence[-1]) < len(input_batch_sequence[-2]):
                input_batch_sequence.pop(-1)
                target_batch_sequence.pop(-1)
            return input_batch_sequence, target_batch_sequence
        except IndexError:
            sequence_folder = os.path.basename(os.path.dirname(input_batch_sequence[0][0]))  # first file of first batch
            logging.info(
                f'Sequence folder "{sequence_folder}" does not contain enough samples. Skipping this sequence.'
            )
            return [], []

    def shuffle_batches(self):
        if not self.input_batches or not self.target_batches:
            raise RuntimeError('Input batches and target batches are not instantiated!')
        combined = list(zip(self.input_batches, self.target_batches))
        random.shuffle(combined)
        self.input_batches = [element[0] for element in combined]
        self.target_batches = [element[1] for element in combined]

    @staticmethod
    def load_sequence_targets_to_tensor(target_paths):
        masks = []
        for mask in target_paths:
            masks.append(torch.tensor(cv2.imread(mask, cv2.IMREAD_GRAYSCALE)))
        return torch.stack(masks)

    def __getitem__(self, item):
        if item == 0 and self.shuffle:
            # shuffles at every new iteration if shuffle is active
            self.shuffle_batches()

        input_batch, target_batch = self.load_batch_tensor(item)
        return input_batch, target_batch

    def __len__(self):
        return len(self.input_batches)


class LoaderBase3D(LoaderBase):
    def __init__(self,
                 input_paths,
                 target_paths,
                 batch_size,
                 classes,
                 channels,
                 depth_or_sequence_size,
                 shuffle=True,
                 drop_last=True):
        super(LoaderBase3D, self).__init__(shuffle)

        if not isinstance(input_paths, list) or not isinstance(target_paths, list):
            raise ValueError('Images and mask paths need to be lists')

        if not isinstance(input_paths[0], list) or not isinstance(target_paths[0], list):
            raise ValueError('Images and mask need to contain sublist representing sequences or volumes')

        self.images = input_paths
        self.masks = target_paths
        self.batch_size = batch_size
        self.channels = channels
        self.classes = classes
        self.drop_last = drop_last
        self.depth_or_sequence_size = depth_or_sequence_size

        if channels == 1:
            self.img_transform = get_gray_image_transformation()
        elif channels == 3:
            self.img_transform = get_rgb_image_transformation()
        else:
            raise NotImplementedError(f'Can not load images with {channels} channels.')

        self.input_batches = []
        self.target_batches = []
        self.create_batches()
        # work around to ensure compatibility with pytorch DataLoader
        self.dataset = FakeDataset(self.input_batches)

    def load_sequence_inputs_to_tensor(self, image_paths):
        raise NotImplementedError

    def load_batch_tensor(self, item):
        input_data = self.input_batches[item]
        target_data = self.target_batches[item]

        input_batch = []
        target_batch = []

        for image_sequence in input_data:
            input_batch.append(self.load_sequence_inputs_to_tensor(image_sequence))
        for image_sequence in target_data:
            target_batch.append(self.load_sequence_targets_to_tensor(image_sequence))
        # Volumes: [B, C, D, H, W], [B, D, H, W]
        # Sequences: [B, F, C, H, W], [B, F, H, W]
        return torch.stack(input_batch), torch.stack(target_batch)

    def create_volume_or_rnn_sequences(self):
        """
        Creates sequences of length self.depth_or_sequence_size where each sequence represents a volume or a sequence

        Returns: input_batch_sequences, target_batch_sequences
            Structure:
                List of sub lists, where each sub list contains self.depth_or_sequence_size images.
                Each batch is a list of file paths, not loaded images (ram usage!)

            Example:
                input_batch_sequences = [sequence_1, sequence_2, ...]
                sequence_1 = [file_path_1, file_path_2, ...]

        """

        # List of sub lists, where each sub list contains self.depth_or_sequence_size images.
        # Each sub list is a list of file paths and not loaded images (ram usage!)

        input_batch_sequences = []
        target_batch_sequences = []

        for seq_nr, image_sequence in enumerate(self.images):
            mask_sequence = self.masks[seq_nr]

            input_batch_sequence = []
            target_batch_sequence = []

            current_input_batch = []
            current_target_batch = []
            for idx, file in enumerate(image_sequence):
                current_input_batch.append(file)
                current_target_batch.append(mask_sequence[idx])

                if (idx + 1) % self.depth_or_sequence_size == 0:
                    # if too less images in a sequence, this will never reached!
                    # this is a wanted behaviour
                    input_batch_sequence.append(current_input_batch)
                    target_batch_sequence.append(current_target_batch)
                    current_input_batch = []
                    current_target_batch = []

            if input_batch_sequence:
                input_batch_sequences.append(input_batch_sequence)
                target_batch_sequences.append(target_batch_sequence)

        return input_batch_sequences, target_batch_sequences

    def create_batches(self):
        """
        A batch is a batch_size number of volumes or sequences
        """
        self.input_batches = []
        self.target_batches = []

        # split into sequences, each of size self.depth (each sequence represents a volume)
        input_batch_sequences, target_batch_sequences = self.create_volume_or_rnn_sequences()
        # batch sequence explained:
        # you passed list of sequences, where each sequence represents a volume.
        # create_batch_sequences() splits each volume in sub volumes of a given depth
        # the _batch_sequences containing now:
        # n sub lists with n = initial volumes
        # one sub list contains m sub lists where m = sub volumes created out of one initial volume
        # example:
        # top level: batch_sequences = [vol_1, vol_2, vol_3, ..]
        # second level: vol_1 = [sub_1_vol_1, sub_2_vol_1, ...]
        # sub_1_vol_1 = [img_1, img_2, ..., img_depth],

        batch_vol_count = 0
        current_input_batch = []
        current_target_batch = []
        for input_volumes, target_volumes in zip(input_batch_sequences, target_batch_sequences):
            for input_volume, target_volume in zip(input_volumes, target_volumes):
                current_input_batch.append(input_volume)
                current_target_batch.append(target_volume)
                batch_vol_count += 1
                if batch_vol_count % self.batch_size == 0:
                    self.input_batches.append(current_input_batch)
                    self.target_batches.append(current_target_batch)
                    current_input_batch = []
                    current_target_batch = []
                    batch_vol_count = 0

        # append last "incomplete" iteration as well
        if not self.drop_last and current_input_batch:
            self.input_batches.append(current_input_batch)
            self.target_batches.append(current_target_batch)

        self.dataset = FakeDataset(self.input_batches)


class VolumeLoader(LoaderBase3D):
    def __init__(self,
                 input_paths,
                 target_paths,
                 batch_size,
                 classes,
                 channels,
                 depth,
                 shuffle=True,
                 drop_last=False):
        super(VolumeLoader, self).__init__(
            input_paths, target_paths, batch_size, classes, channels, depth, shuffle, drop_last
        )

        self.depth_or_sequence_size = depth
        self.create_batches()

    def load_sequence_inputs_to_tensor(self, image_paths):
        """
        Using opencv is faster than PIL
        """
        images = []

        if self.channels == 1:
            for image in image_paths:
                # load img, convert to tensor, add to list
                # each img is transformed to be shaped like [C, H, W]
                images.append(self.img_transform(cv2.imread(image, cv2.IMREAD_GRAYSCALE)))
        elif self.channels == 3:
            for image in image_paths:
                # converting bgr to rgb
                images.append(self.img_transform(cv2.cvtColor(cv2.imread(image, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)))
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        # [D, C, H, W] --> [C, D, H, W]
        return torch.transpose(torch.stack(images), 0, 1)


class BatchedSequenceLoader(LoaderBase3D):
    """
    Custom data loader, which loads batches where each batch contains F frames resulting in an
    input tensor shaped like:
        [B F C H W]
    and an target tensor shaped like:
        [B F H W]

    Example: Frames in a Video, 100 frames in total, B = 4, F = 5, shuffle is true
        First slice into 4 lists of size: 100/B = 25
        Now slice each batch into: sequences containing 5 frames: 25/F = 5
        Shuffle does only shuffle the batches and the order of the 5 frame sequences,
        not the sequences themself
    """
    def __init__(self,
                 input_paths,
                 target_paths,
                 batch_size,
                 classes,
                 channels,
                 rnn_sequence_size,
                 shuffle=True,
                 drop_last=False):

        super(BatchedSequenceLoader, self).__init__(
            input_paths, target_paths, batch_size, classes, channels, rnn_sequence_size, shuffle, drop_last
        )

        self.rnn_sequence_size = rnn_sequence_size

        self.input_batches = []
        self.target_batches = []
        self.create_batches()

    def load_sequence_inputs_to_tensor(self, image_paths):
        """
        Using opencv is faster than PIL
        """
        images = []

        if self.channels == 1:
            for image in image_paths:
                # load img, convert to tensor, add to list
                # each img is transformed to be shaped like [C, H, W]
                images.append(self.img_transform(cv2.imread(image, cv2.IMREAD_GRAYSCALE)))
        elif self.channels == 3:
            for image in image_paths:
                # converting bgr to rgb
                images.append(self.img_transform(cv2.cvtColor(cv2.imread(image, cv2.IMREAD_COLOR), cv2.COLOR_BGR2RGB)))
        else:
            raise NotImplementedError(
                f'Trying to load batch with {self.channels} channels. This is not supported')

        # [F, C, H, W]
        return torch.stack(images)
