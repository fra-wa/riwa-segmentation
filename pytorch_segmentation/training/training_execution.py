import logging
import torch

from .. import constants
from ..utils import show_memory


def get_targets_and_outputs(inputs, targets, params, model):
    """
    Used by test loop
    Returns output batch with channels always at out_batch.shape[1]
    """
    inputs = inputs.to(params.device, dtype=torch.float)

    # long for multiclass else we could get too large numbers resulting in nan
    masks_type = torch.float32 if params.classes == 1 else torch.long
    targets = targets.to(params.device, dtype=masks_type)
    if params.architecture in constants.STATEFUL_RECURRENT_UNETS:
        outputs = model(inputs, True)
        # B, F, C, H, W -> B, C, F, H, W
        outputs = torch.transpose(outputs, 1, 2)
    elif params.architecture in constants.STATELESS_RECURRENT_UNETS:
        # not tested since 24.06.2021! Dropped support of last stateless network

        # stateless uses for example the previous 5 images to predict the current mask.
        # 5 in that case is the rnn_sequence_size --> skip 4 first images, predict 5th
        outputs = model(inputs)
        targets = targets[params.rnn_sequence_size:]
    else:
        outputs = model(inputs)
    return targets, outputs


def get_targets_outputs_and_loss(input_batch, target_batch, params, model, loss_func):
    """
    Used by train and validation loop
    Returns output batch with channels always at out_batch.shape[1]
    """
    input_batch = input_batch.to(params.device, dtype=torch.float)

    # long for multiclass else we could get too large numbers resulting in nan
    masks_type = torch.float32 if params.classes == 2 else torch.long
    target_batch = target_batch.to(params.device, dtype=masks_type)

    if params.architecture in constants.STATEFUL_RECURRENT_UNETS:
        output_batch = model(input_batch, True)
        # B, F, C, H, W -> B, C, F, H, W
        output_batch = torch.transpose(output_batch, 1, 2)
    elif params.architecture in constants.STATELESS_RECURRENT_UNETS:
        # not tested since 24.06.2021! Dropped support of last stateless network

        # stateless uses for example the previous 5 images to predict the current mask.
        # 5 in that case is the rnn_sequence_size --> skip 4 first images, predict 5th
        output_batch = model(input_batch)
        target_batch = target_batch[params.rnn_sequence_size:]
    else:
        output_batch = model(input_batch)

    if params.classes == 2:
        loss = loss_func(output_batch.squeeze(dim=1), target_batch)
    else:
        loss = loss_func(output_batch, target_batch)

    return target_batch, output_batch, loss


def compute_accuracy(output_logits, targets):
    # output_logits of shape: B, C, ....
    if output_logits.size(1) == 1:
        output_probabilities = torch.sigmoid(output_logits)
        outs = (output_probabilities.squeeze(1) > 0.5).float()
        correct = outs.eq(targets.squeeze()).sum().item()
    else:
        values, predictions = torch.max(output_logits, dim=1)
        correct = predictions.eq(targets).sum().item()

    total = targets.nelement()  # sums up all pixels (batch_size x width x height)
    accuracy = (correct / total) * targets.size(0)

    return accuracy


def train_loop(params, model, training_loader, loss_func, optimizer, epoch, epochs):
    model.train()

    train_loss = 0.0
    train_acc = 0.0

    logging.info('Training loop:')

    try:
        for i, (input_batch, target_batch) in enumerate(training_loader):
            logging.info(f'Epoch {epoch + 1}/{epochs}, train loop: Batch {i + 1} / {len(training_loader)}')
            if i <= 3 or i % 30 == 0:
                show_memory(params.device)

            # Setting gradient to None has a slightly different numerical behavior than setting it to zero
            # see: https://pytorch.org/docs/master/optim.html#torch.optim.Optimizer.zero_grad
            optimizer.zero_grad(set_to_none=True)

            targets, outputs, loss = get_targets_outputs_and_loss(input_batch, target_batch, params, model, loss_func)

            # if last batch has less elements, last batch must be weighted different. Therefore: loss x batch_size
            train_loss += loss.item() * targets.size(0)
            train_acc += compute_accuracy(outputs, targets)

            loss.backward()
            optimizer.step()
    except (RuntimeError, ValueError) as e:
        non_intuitive_vram_errors = [
            'CUDA out of memory',
            'CUDA error: out of memory',
            'Unable to find a valid cuDNN algorithm to run convolution',
            'cuDNN error: CUDNN_STATUS_NOT_SUPPORTED',
        ]
        for non_intuitive_error in non_intuitive_vram_errors:
            if non_intuitive_error in e.args[0]:
                raise RuntimeError(f'\n!!! Not enough memory to compute !!!\nError: {e}') from e
        raise RuntimeError(f'Got an error during training:\n{e}') from e

    # loss was multiplied by each batch size --> divide by all training samples
    train_acc /= len(training_loader.dataset)
    train_loss /= len(training_loader.dataset)
    
    return train_loss, train_acc, model


def valid_loop(params, model, validation_loader, loss_func, epoch, epochs):
    valid_loss = 0.0
    valid_acc = 0.0

    model.eval()
    with torch.no_grad():
        logging.info('Validation loop:')
        for i, (input_batch, target_batch) in enumerate(validation_loader):
            logging.info(f'Epoch {epoch + 1}/{epochs}, validation loop: Batch {i + 1} / {len(validation_loader)}')
            if i <= 3 or i % 30 == 0:
                show_memory(params.device)

            targets, outputs, loss = get_targets_outputs_and_loss(input_batch, target_batch, params, model, loss_func)

            valid_loss += loss.item() * targets.size(0)
            valid_acc += compute_accuracy(outputs, targets)

    valid_acc /= len(validation_loader.dataset)
    valid_loss /= len(validation_loader.dataset)

    return valid_loss, valid_acc, model
