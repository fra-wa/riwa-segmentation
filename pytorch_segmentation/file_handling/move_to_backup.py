import os
import shutil

from .utils import create_folder
from .utils import get_sub_folders


def move_to_backup_folder(src, backup_folder, max_backups=5, create_empty_src=True):
    """
    Saves the source folder to a backup folder.

    Args:
        src: folder to move into backup
        backup_folder: main folder of backups
        max_backups: max number of stored backups (backup folder contains: backup_1, backup_x, ..)
            if max number is reached, the oldest backup will be deleted
        create_empty_src: whether to create a new src folder or move it completely

    Examples:
        You have an output folder and do not want to accidentally delete everything in it

    """
    create_folder(backup_folder)
    backup_folders = get_sub_folders(backup_folder, depth=1)
    folder_names = [os.path.basename(folder) for folder in backup_folders]
    numbers = []
    for name in folder_names:
        try:
            numbers.append(int(name.split('_')[1]))
        except (ValueError, IndexError):
            pass
    try:
        latest = max(numbers)
    except ValueError:
        latest = 0

    new_folder_name = f'backup_{latest + 1}'
    dst = os.path.join(backup_folder, new_folder_name)

    if len(numbers) >= max_backups:
        oldest = min(numbers)
        oldest_folder_name = f'backup_{oldest}'
        shutil.rmtree(os.path.join(backup_folder, oldest_folder_name))

    shutil.move(src, dst)
    if create_empty_src:
        create_folder(src)
