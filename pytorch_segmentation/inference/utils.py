import numpy as np
import torch

from pytorch_segmentation.inference import inference_constants


def decode_logits(out_logits, classes, binary_logit_threshold=0.5):
    """Assumption: Only one class is true not multiple"""
    if isinstance(out_logits, np.ndarray):
        out_logits = out_logits.astype(np.float32)
        out_logits = torch.from_numpy(out_logits)

    if classes == 1:
        # binary classification image: out_logits of shape: B, C, H, W
        # binary classification volume: out_logits of shape: B, 1, D, H, W
        out = torch.sigmoid(out_logits)
        segmentation_map = (out.squeeze(dim=1) > binary_logit_threshold).float().cpu().numpy()
        classes = 2

    else:
        # img: out_logits of shape: B, C, H, W
        # vol: out_logits of shape: B, C, D, H, W
        values, predictions = torch.max(out_logits, dim=1)
        # img: predictions of shape: B, H, W
        # vol: predictions of shape: predictions of shape: B, D, H, W
        segmentation_map = predictions.cpu().numpy()

    return segmentation_map, classes


def create_decoding_array(classes):
    """Important to have here: also needed during testing!"""
    # mask contains values from 0 to 255 like:
    decoding_array = np.arange(0, classes)
    decoding_array = (decoding_array / (classes - 1)) * 255
    decoding_array = np.round(decoding_array)
    decoding_array[decoding_array > 255] = 255
    decoding_array = decoding_array.astype(np.uint8)
    return decoding_array


def map_to_gray(class_map, class_count):
    decoding_array = create_decoding_array(class_count)
    gray_map = np.zeros_like(class_map).astype(np.uint8)
    for class_number in range(class_count):
        idx = class_map == class_number
        gray_map[idx] = decoding_array[class_number]

    return gray_map


def map_to_color(class_map, class_count):
    """Helper for decode_segmentation_map"""
    decoding_map = inference_constants.BEAUTIFUL_COLORS_RGB

    r = np.zeros_like(class_map).astype(np.uint8)
    g = np.zeros_like(class_map).astype(np.uint8)
    b = np.zeros_like(class_map).astype(np.uint8)

    for class_number in range(class_count):
        idx = class_map == class_number
        r[idx] = decoding_map[class_number, 0]
        g[idx] = decoding_map[class_number, 1]
        b[idx] = decoding_map[class_number, 2]

    if len(class_map.shape) == 4:
        colored_map = np.stack([b, g, r], axis=4)
    elif len(class_map.shape) == 3:
        colored_map = np.stack([b, g, r], axis=3)
    elif len(class_map.shape) == 2:
        colored_map = np.stack([b, g, r], axis=2)
    else:
        raise RuntimeError(f'Invalid shape of class_map: {class_map.shape}')

    return colored_map


def decode_segmentation_map(segmentation_map, class_count, decode_to_color):
    """
    0 in segmentation map relates to background! If not, you need to change.

    Args:
        segmentation_map: either a batch of volumes or a single volume or a stack of 2D images
            shape for volumes:
                (b, d, h, w) or (d, h, w)
            shape for images:
                (b, h, w) or (h, w)
        class_count: max classes, which the model can predict (at least 2 because true or false)
        decode_to_color: clear.

    Returns: decoded image(s) or volume(s)
        shapes if not decoded to color: mapped between 0 and 255
        shapes if decoded to color:
            shape for volumes:
                (b, d, h, w, 3) or (d, h, w, 3)
            shape for images:
                (b, h, w, 3) or (h, w, 3)

    """
    if not isinstance(segmentation_map, np.ndarray):
        raise ValueError('Segmentation map must be a numpy array')

    if decode_to_color:
        if class_count > inference_constants.BEAUTIFUL_COLORS_RGB.shape[0]:
            raise RuntimeError('Cant detect that many classes, increase decoding labels')

        decoded_stack = map_to_color(segmentation_map, class_count)
    else:
        decoded_stack = map_to_gray(segmentation_map, class_count)

    return decoded_stack


def __expand_1d_weight_to_3d(weight_array, height, width):
    """
    What it does:
    Step 1:
        assert a volume of depth = 5 --> linear weight would be: [1,2,3,2,1]
        this will be reshaped to:
        [[1],
         [2],
         [3],
         [2],
         [1]]
    Step 2:
        then expanded to the height of for example 3:
        [[1, 1, 1],
         [2, 2, 2],
         [3, 3, 3],
         [2, 2, 2],
         [1, 1, 1]]
        and finally repeated "into" the monitor width times to get a 3d weight out of the input array.
    """
    depth = weight_array.shape[0]
    weight_slice = weight_array.reshape(depth, 1).repeat(height, axis=1)  # Step 2: 2D: depth, height
    weight_volume = weight_slice.reshape(depth, height, 1).repeat(width, axis=2)  # --> shape: depth, height, width
    return weight_volume


def get_logits_weight(mode, height, width, depth=None):
    """

    Args:
        mode: 'sum', 'linear', 'gauss'
        height:
        width:
        depth:
    Examples:
        gauss weighting: trying to get ~1/3 at border
            height of 2:    min: 0.336; max: 1.000
            height of 128:  min: 0.330; max: 1.000
            height of 256:  min: 0.328; max: 1.000
            height of 1000: min: 0.325; max: 1.000

    Returns:

    """
    weight_modes = ['sum', 'linear', 'gauss']
    if mode not in weight_modes:
        raise ValueError(f'Weight mode must be one of: {", ".join(weight_modes)}')
    if height != width and mode != 'sum':
        raise ValueError('logits height != width. Weighting can only use sum. Implement for different shapes.')

    if mode == 'sum':
        if depth is not None:
            shape = (depth, height, width)
        else:
            shape = (height, width)
        weight = np.ones(shape)
    elif mode == 'linear':
        assert height == width
        ranged = np.arange(1, width + 1)
        weight_array = np.minimum(ranged, ranged[::-1])
        weight = np.minimum.outer(weight_array, weight_array)
        if depth is not None:
            ranged = np.arange(1, depth + 1)
            weight_array = np.minimum(ranged, ranged[::-1])
            depth_weight = __expand_1d_weight_to_3d(weight_array, height, width)
            weight = np.array([weight] * depth)
            weight = weight * depth_weight

    elif mode == 'gauss':
        assert height == width
        # weight outer values 1/3 of inner
        sigma = height / 3  # results into: h = 8: min 0.42, max 0.98; h = 100000: min=0.32, max=1.0
        ax_hw = np.linspace(-(height - 1) / 2., (height - 1) / 2., height)
        gauss = np.exp(-0.5 * np.square(ax_hw) / np.square(sigma))
        weight = np.outer(gauss, gauss)

        if depth is not None:
            # expand to third dimension using a gauss distribution over the depth
            # if h and w are not similar, this step may be repeated over the other two axis.
            sigma = depth / 3
            ax_d = np.linspace(-(depth - 1) / 2., (depth - 1) / 2., depth)
            gauss = np.exp(-0.5 * np.square(ax_d) / np.square(sigma))  # 1D array
            depth_weight = __expand_1d_weight_to_3d(gauss, height, width)
            weight = np.array([weight] * depth)  # weight volume for each slice in the logits volume
            weight = depth_weight * weight
        weight = np.round(weight * 100, 0)

    else:
        raise RuntimeError('Weight mode not supported')

    return weight
