# RIWA Segmentation Paper - Code and more

**This project does not get any updates!** 
This version reflects the state of the published paper.
For an updated and further developed version, see [AiSeg](https://gitlab.com/fra-wa/aiseg)

This project contains the source code of the paper. 
It also contains some parameters used to reproduce the results (see: 8. [Paper parameters](#paper)).


---

| Features                                                                                                                                                                                                                                                                                                                             | Interface                                                                     |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| - **User Interface** (see right)<br>- **35 2D and 9 3D supported models** (more with different backbones)<br>- **Need a model?** Simply [copy](#copy) it (cite the authors and this project!)<br>- **Data augmentation** (online and offline) <br>- **Training**<br>- **Inference**<br>- **Analysis**<br>- **Easy installation**<br> | <img src="/readme_images/comparison_graphs.png" alt="interface" width="500"/> |

# Table of Contents:

---

1. [**Background**](#background)
2. [**Installation**](#install)
3. [**Tutorial**](#tutorial)
4. [**Models**](#archi)
    - [Overview](#overview)
        - [2D](#overview2d)
        - [3D](#overview3d)
5. [**Data preparation**](#data)
6. [**Augmentation**](#augmentation)
7. [**Citation**](#citation)
8. [**Paper**](#paper)

<a name="background"></a>
# Background
<details>
<summary><b><u>Click to expand</u></b></summary>

---
#### Project info
You want to use this project or code of it for commercial use? Please contact 
[Franz Wagner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/photogrammetrie/die-professur/beschaeftigte/m-sc-franz-wagner?set_language=en)

#### Why it was created
This project was initially created in the scope of [CRC/TRR 280](https://www.sfbtrr280.de/en/) to segment cement-based 
composites captured with a microtomograph. One question was: "Which model works best for this task?". So I implemented 
"some" state-of-the art networks and now this project contains one of the largest collection of2D and 3D segmentation 
models (UPDATE: found [PaddleSeg](https://github.com/PaddlePaddle/PaddleSeg). It contains more but no such UI). 
Further, this project has an optimized (in terms of best practice) training, inference and augmentation module as 
well as an UI.

#### About me
A former software developer, now working at the TU Dresden as research associate (and PhD student) at the chair of 
photogrammetry.
</details>

<a name="install"></a>
# Installation

<details>
<summary><b><u>Click to expand</u></b></summary>

**Hardware requirements:**
- at least 2 core CPU (4 or more recommended)
- at least 12GB RAM (16 or more recommended)
- NVIDIA GPU with at least 4GB RAM for training, 2GB for inference (Recommended: training: 8GB or more, inference: 4GB or more)

**Windows:**
1. Install Python 3.8 (Download here: https://www.python.org/ftp/python/3.8.10/python-3.8.10-amd64.exe)
2. execute the `windows_installer.bat` (this creates a virtual environment inside this project)

**Linux (Ubuntu)**
1. Use Python 3.8, ensure pip and venv are installed!
2. open a terminal and cd to this project
3. call: `source linux_installer.sh` this creates a folder called virtual_env at the same level as this file.

**Uninstall?** Simply delete the project.
</details>

# Start the UI
<details>
<summary><b><u>Click to expand</u></b></summary>

**Windows**
1. double-click on the `start_server.bat`

**Linux**
1. open a terminal and cd to `../pytorch_segmentation/`
2. call: `source start_server.sh`

<a name="tutorial"></a>
</details>

# Tutorial

This software contains a prefilled database. If you want a clean install, simply delete the `db.sqlite` file inside the `\db`
folder (it is also stored in the `\databases_prefilled` directory).

<details>
<summary><b><u>General</u></b></summary>

**Basic layout**

<img src="/readme_images/tutorial/index.PNG" alt="index" width="800"/>

**The navigation bar explained**

<img src="/readme_images/tutorial/navbar.PNG" alt="index" width="800"/>
</details>

<details>
<summary><b><u>Training</u></b></summary>

<img src="/readme_images/tutorial/training/index_training.png" alt="interface" width="800"/>

**Click on Training or use a checkpoint to continue the training**

<img src="/readme_images/tutorial/training/overview.PNG" alt="tr_overview" width="800"/>

**The training form**

<img src="/readme_images/tutorial/training/training/training_form.PNG" alt="tr_form" width="800"/>

1. **Select your Dataset**
2. **Select the Architecture** (UNetResNet with ResNeXt 50 backbone is a good start)
3. **Normalization** if your batch size is <=16, select Group Normalization!
4. **Input channels** 3 for rgb, 1 for gray
5. **Output classes** 2 for binary
6. **Pretrained** True is in most cases superior! weights pretrained on imagenet
7. **Use online augmentation** see at augmentation.
8. **Load or calculate optimal parameters** 
   - Calculates optimal parameters for your system. 
   - Tries to use a batch size of >=16 by reducing the input size
   - However: 
     - **larger input size is superior**! 
     - If you think, it is too small: increase input size and
     - use Group Normalization ([paper](https://arxiv.org/pdf/1803.08494.pdf]))
     - Set largest possible input size and a batch size of at least 2
   - Calculate with fixed depth: only for 3D. Calculation does use your set depth.
9. **Or set a custom input size**
   - some models expect input sizes of 2^n, where n >= 5
   - some 3D models (Like UNet 3D) expect input depths which are dividable by 2 at least 4 times 
     - smallest input depth is therefore: 16 (16/(2^4)=1)
   - Larger input sizes are superior!

**Monitoring** (all log outputs are stored at: `pytorch_segmentation/logs/`)
- live logging is showed after you hit run:

<img src="/readme_images/tutorial/training/training/training_run_with_log.png" alt="tr_logging" width="800"/>

</details>

<details>
<summary><b><u>Inference</u></b></summary>

<img src="/readme_images/tutorial/inference/index_inference.png" alt="tr_logging" width="800"/>

**The inference form**

<img src="/readme_images/tutorial/inference/inference.png" alt="tr_logging" width="800"/>

1. **Pass the path to your checkpoint**
2. **Pass the path to the data you want to segment**
3. **Select overlapping (50%: every pixel is segmented at least twice)**
4. **Choose a weighting strategy if overlap is active**
    - Sum: Each segmentation has a weight of 1
    - Linear: Border pixels have a weight of 1, increasing linear by 1 to the center (like a pyramid in 2D)
    - Gauss: 2D/3D gaussian bell where border pixels have a weight of ~1/3 to that of the center

</details>

<details>
<summary><b><u>Augmentation</u></b></summary>

<details>
<summary><b><u>Online Augmentation</u></b></summary>

**Select online augmentation** during training initialization and prepare the expected input size (and depth for 3D)

<img src="/readme_images/tutorial/augmentation/online_aug/online_aug_active.jpg" alt="onl_aug_init" width="800"/>

**A new view:**

<img src="/readme_images/tutorial/augmentation/online_aug/online_aug_interface.jpg" alt="onl_aug_form" width="800"/>

1. **Set your parameters**
2. **Check your parameters by clicking on Show examples (top right)**:

**See examples of your set parameters**

<img src="/readme_images/tutorial/augmentation/online_aug/online_aug_examples.jpg" alt="onl_aug_examples" width="800"/>
</details>

<details>
<summary><b><u>Offline Augmentation</u></b></summary>

<img src="/readme_images/tutorial/augmentation/index_augmentation.png" alt="aug_index" width="800"/>

**Set your parameters, choose the augmentation methods and run:**

<img src="/readme_images/tutorial/augmentation/offline/image_aug_form.PNG" alt="offl_aug" width="800"/>
</details>
</details>


<details>
<summary><b><u>Model Analysis</u></b></summary>

<img src="/readme_images/tutorial/analysis/index_analysis.png" alt="analysis_index" width="800"/>

<details>
<summary><b><u>Single Model</u></b></summary>

**Overview of your trained models** on top you can export or clean up those used for hyperparameter testing

<img src="/readme_images/tutorial/analysis/overview.jpg" alt="analysis_overview" width="800"/>

**Select a model and see the analysis**

<img src="/readme_images/tutorial/analysis/analysis_1.jpg" alt="analysis_1" width="800"/>

**Check out the used online augmentation parameters if used**

<img src="/readme_images/tutorial/analysis/analysis_2.jpg" alt="analysis_2" width="800"/>

**Check out the single class performance of your network**

<img src="/readme_images/tutorial/analysis/analysis_3.jpg" alt="analysis_3" width="800"/>

**See fancy graphs of your training run** you can also export the model data (bottom right)

<img src="/readme_images/tutorial/analysis/analysis_4.jpg" alt="analysis_4" width="800"/>

**Export: exports to a folder**

<img src="/readme_images/tutorial/analysis/export.jpg" alt="export_single" width="800"/>
</details>

<details>
<summary><b><u>Model Comparison</u></b></summary>

**Comparison**

<img src="/readme_images/tutorial/comparison/comparison_1_annotated.jpg" alt="comparison_1" width="800"/>

**Select all networks** might be too much

<img src="/readme_images/tutorial/comparison/comparison_2.jpg" alt="comparison_2" width="800"/>

**Show only the bets X networks** activate sorting by a datasets (bottom left)

<img src="/readme_images/tutorial/comparison/comparison_3.jpg" alt="comparison_3" width="800"/>

**Search for your networks of interest**

<img src="/readme_images/tutorial/comparison/comparison_4.jpg" alt="comparison_4" width="800"/>

**Click on a network to activate/deactivate it**

<img src="/readme_images/tutorial/comparison/comparison_5.jpg" alt="comparison_5" width="800"/>
</details>

</details>

<details>
<summary><b><u>Misc</u></b></summary>

<details>
<summary><b><u>Import Models</u></b> trained on a different machine</summary>

**searches also in subdirectories if wanted** (an existing dataset is needed!)

<img src="/readme_images/tutorial/misc/import_1.jpg" alt="import" width="800"/>

1. select or create a dataset. All imported nets are related to this dataset!
2. pass the path to your folder containing the checkpoint(s)
3. only interested in analysis? import only the last checkpoint of your folder

</details>

<details>
<summary><b><u>Datasets</u></b></summary>

- **Will be created/related automatically if a new training is started**
- **A dataset is related to a single folder on the device!** you can set a custom name for convenience

**Show datasets**

<img src="/readme_images/tutorial/misc/datasets.jpg" alt="dataset" width="800"/>

**Or create a new one**

<img src="/readme_images/tutorial/misc/dataset_new.jpg" alt="create_dataset" width="800"/>
</details>

<details>
<summary><b><u>Configurations</u></b></summary>

**Left: calculated training configurations | Right: optimal inference configurations**

<img src="/readme_images/tutorial/misc/configurations.jpg" alt="configs" width="800"/>
</details>

<details>
<summary><b><u>Logs</u></b></summary>

- **Logs are created automatically**
- **They are stored at: `pytorch_segmentation/logs`**
- **every execution has their own log**

<img src="/readme_images/tutorial/misc/logs_1.jpg" alt="logs" width="800"/>

**Select a log, and you get to the monitoring**

<img src="/readme_images/tutorial/training/training/training_run_with_log.png" alt="example_log" width="800"/>
</details>

<details>
<summary><b><u>Monitor</u></b></summary>

**Every execution can be monitored live, like a training:**

<img src="/readme_images/tutorial/training/training/training_run_with_log.png" alt="example_log_tr" width="800"/>

**Or a testing**

<img src="/readme_images/tutorial/misc/logs_example_testing.jpg" alt="example_log_tr" width="800"/>

**Or a parameter calculation**

<img src="/readme_images/tutorial/misc/logs_example_para_calc.jpg" alt="example_log_tr" width="800"/>

**And so on**
</details>

</details>

<a name="archi"></a>
# Models

<details>
<summary><b><u>Click to expand</u></b></summary>

<a name="overview"></a>
**Overview**
- If **source is given**, the model is not implemented by myself but copied, bug freed and the code is changed more or 
  less to fit into this project (GroupNorm was also added).
- If **paper is given**, I reimplemented the network or due to bugs in the original implementation and/or I heavily 
  refactored a network code.
- If **a backbone** is given, you can pass one of them during the training guide.

<a name="overview2d"></a>
### 2D Networks

<details>
<summary><b><u>Click to show 2D networks</u></b></summary>

| Model Name                     | Possible Backbones                                                        | Source                                                                                                   |
|--------------------------------|---------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| Attention UNet                 |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation)                                   |
| BiSeNet                        |                                                                           | [source: zllrunning](https://github.com/zllrunning/face-parsing.PyTorch)                                 |
| BiSeNetV2                      |                                                                           | [source: CoinCheung](https://github.com/CoinCheung/BiSeNet)                                              |
| DANet                          | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: junfu1115](https://github.com/junfu1115/DANet)                                                  |
| DRANet                         | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: junfu1115](https://github.com/junfu1115/DANet)                                                  |
| DeepLab V3+                    | drn, xception, resnet101                                                  | [source: jfzhang95](https://github.com/jfzhang95/pytorch-deeplab-xception)                               |
| DenseASPP 121/161/169/201      |                                                                           | [source: DeepMotionAIResearch](https://github.com/DeepMotionAIResearch/DenseASPP)                        |
| DenseNet 57/67/103             |                                                                           | [source: ilsang](https://github.com/ilsang/PyTorch-SE-Segmentation)                                      |
| DFANet                         |                                                                           | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch)                        |
| DFN                            |                                                                           | [source: ycszen](https://github.com/ycszen/TorchSeg)                                                     |
| DeepLab DUC - HDC              |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation)                                   |
| ENet                           |                                                                           | [source: yassouali](https://github.com/yassouali/pytorch-segmentation)                                   |
| ERFNet                         |                                                                           | [source: Eromera](https://github.com/Eromera/erfnet_pytorch)                                             |
| ESPNet                         |                                                                           | [source: sacmehta](https://github.com/sacmehta/ESPNet)                                                   |
| ExtremeC3Net                   |                                                                           | [source: clovaai](https://github.com/clovaai/ext_portrait_segmentation)                                  |
| Fast-SCNN                      |                                                                           | [source: DeepVoltaire](https://github.com/DeepVoltaire/Fast-SCNN)                                        |
| FCN 8/16/32                    |                                                                           | [source: wkentaro](https://github.com/wkentaro/pytorch-fcn)                                              |
| GCN                            | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | [source: yassouali](https://github.com/yassouali/pytorch-segmentation)                                   |
| GSCNN                          | wider_resnet38_a2 (not changeable)                                        | [source: nv-tlabs](https://github.com/nv-tlabs/GSCNN)                                                    |
| HRNet                          |                                                                           | [source: HRNet](https://github.com/HRNet/HRNet-Semantic-Segmentation)                                    |
| ICNet                          | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch)                        |
| LadderNet                      |                                                                           | [source: juntang-zhuang](https://github.com/juntang-zhuang/LadderNet)                                    |
| LEDNet                         |                                                                           | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch)                        |
| OCNet                          | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: Tramac](https://github.com/Tramac/awesome-semantic-segmentation-pytorch)                        |
| PSANet (currently not working) | resnet50, resnet101, resnet152, resnext50, resnext101                     | [paper](https://arxiv.org/ftp/arxiv/papers/2102/2102.07880.pdf)                                          |
| PSPNet                         | resnet50, resnet101, resnet152, resnext50, resnext101                     | Refactored, added better decoding [source: yassouali](https://github.com/yassouali/pytorch-segmentation) |
| PSPDenseNet                    | densenet121, densenet161, densenet169, densenet201                        | [source: yassouali](https://github.com/yassouali/pytorch-segmentation)                                   |
| R2-Attention UNet              |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation)                                   |
| R2UNet                         |                                                                           | [source: LeeJunHyun](https://github.com/LeeJunHyun/Image_Segmentation)                                   |
| SegNet                         | resnet50, resnet101, resnet152, resnext50, resnext101                     | [source: yassouali](https://github.com/yassouali/pytorch-segmentation)                                   |
| SINet                          |                                                                           | [source: clovaai](https://github.com/clovaai/ext_portrait_segmentation)                                  |
| UNet                           | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | Build by myself but not new.                                                                             |
| UNet ++                        |                                                                           | [source: 4uiiurz1](https://github.com/4uiiurz1/pytorch-nested-unet)                                      |
| UNet 3+                        | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | Customized, no code was provided. [paper](https://arxiv.org/ftp/arxiv/papers/2004/2004.08790.pdf)        |
| UPerNet                        | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | [source: yassouali](https://github.com/yassouali/pytorch-segmentation)                                   |

</details>

<a name="overview3d"></a>
### 3D Networks

<details>
<summary><b><u>Click to show 3D networks</u></b></summary>

| Model Name       | Possible Backbones                                                        | Source                                                                  |
|------------------|---------------------------------------------------------------------------|-------------------------------------------------------------------------|
| Dense Voxel Net  |                                                                           | [source: black0017](https://github.com/black0017/MedicalZooPytorch)     |
| High ResNet 3D   |                                                                           | [source: black0017](https://github.com/black0017/MedicalZooPytorch)     |
| ResNet Med 3D    | resnet10 resnet18 resnet34 resnet50 resnet101 resnet152 resnet200         | [source: black0017](https://github.com/black0017/MedicalZooPytorch)     |
| Stateful UNet    | resnet18, resnet34, resnet50, resnet101, resnet152, resnext50, resnext101 | New (2021), but not as good as pure 3D networks! Use only for sequences |
| Skip DenseNet 3D |                                                                           | Bug freed [source: tbuikr](https://github.com/tbuikr/3D-SkipDenseSeg)   |
| Residual UNet 3D |                                                                           | [source: wolny](https://github.com/wolny/pytorch-3dunet)                |
| UNet 3D          |                                                                           | [source: wolny](https://github.com/wolny/pytorch-3dunet)                |
| VNet             |                                                                           | [source: black0017](https://github.com/black0017/MedicalZooPytorch)     |
| VNet Light       |                                                                           | [source: black0017](https://github.com/black0017/MedicalZooPytorch)     |

</details>

<a name="copy"></a>
### How to copy:
To **use them in your project**: copy the base_model.py, the model_constants.py, the utils.py and of course
the related model folder. 

Also: **Cite the authors and the project!** That's it.

Attention: binary classification of background and foreground expects num_classes=2! Change that if needed.

</details>

<a name="data"></a>
# Data preparation

<details>
<summary><b><u>Click to expand</u></b></summary>

**Example images below!** (expand 2D or 3D Data)

Create a folder at the same level as `pytorch_segmentation/` called `segmentation_datasets` and prepare your data as follows:
- **Images and masks** must have the **same name** (not extension, e.g.: image: img_0001.jpg mask: img_0001.png)!
- If(recommended) you have validation data, put your it inside here: `your_dataset_folder/validation`
- If(recommended) you have test data, put your it inside here: `your_dataset_folder/test`
- If you have no explicit validation data, the validation data will be extracted from the training data. To reproduce 
  the separation, set reproduce to True during the guide. I recommend preselecting 
  the validation data to enable training continuation from checkpoints without reproduce=True. 

Here is an example of the folder structure (with validation and test data):

## Setup: 
Model zoo will be created during training and contains your models.

<img src="/readme_images/folder_structure/01_base_dir.PNG" alt="base_dir">

## Put Datasets inside segmentation_datasets

<img src="/readme_images/folder_structure/02_segmentation_datasets.PNG" alt="base_dir">

## Setup your data:

**2D Data**
<details>
<summary><b><u>Click to show folder structure</u></b></summary>

| Datasets                                                                                           | 2D Structure                                                                            | Example Images                                                            |
|----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---------------------------------------------------------------------------|
| <img src="/readme_images/folder_structure/02_segmentation_datasets_select_2d.PNG" alt="structure"> | <img src="/readme_images/folder_structure/03_dataset_structure_2d.PNG" alt="dataset2d"> | <img src="/readme_images/folder_structure/04_images_2d.PNG" alt="images"> |

```
pytorch_segmentation/
segmentation_datasets/
    └───2D_dataset
        └───images
        │   img_0001.jpg
        │   img_0002.jpg
        │   ...
        │
        └───masks
        │   mask_0001.tif
        │   mask_0002.tif
        │   ...
        │ 
        └───validation (optional, otherwise validation will be created from images and masks)
        │    └───images
        │    │   val_img_0001.png
        │    │   val_img_0002.png
        │    │   ...
        │    │
        │    └───masks
        │        val_mask_0001.png
        │        val_mask_0002.png
        │        ...
        │
        └───test (optional, last 5 epochs will be analyzed against this data)
             └───images
             │   test_img_0001.png
             │   test_img_0002.png
             │   ...
             │
             └───masks
                 test_mask_0001.png
                 test_mask_0002.png
                 ...
```
</details>

**3D Data**
<details>
<summary><b><u>Click to show folder structure</u></b></summary>

| Datasets                                                                                           | 3D Structure                                                                            | Images folder contains sub folders with the volumes                                         |
|----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------|
| <img src="/readme_images/folder_structure/02_segmentation_datasets_select_3d.PNG" alt="structure"> | <img src="/readme_images/folder_structure/06_dataset_structure_3d.PNG" alt="dataset2d"> | <img src="/readme_images/folder_structure/08_dataset_structure_images_3d.PNG" alt="images"> |


```
pytorch_segmentation/
segmentation_datasets/
    └───3D_or_time_sequence_dataset
        └───images
        │   └───sequence_or_vol_1
        │   │   img_0001.bmp
        │   │   img_0002.bmp
        │   │   ...
        │   └───sequence_or_vol_2
        │   │   img_0001.jpg
        │   │   img_0002.jpg
        │   │   ...
        │   └───sequence_or_vol_n
        │       img_0001.png
        │       img_0002.png
        │       ...
        └───masks
        │   └───sequence_or_vol_1
        │   │   mask_0001.png
        │   │   mask_0002.png
        │   │   ...
        │   └───sequence_or_vol_2
        │   │   mask_0001.png
        │   │   mask_0002.png
        │   │   ...
        │   └───sequence_or_vol_n
        │       mask_0001.png
        │       mask_0002.png
        │       ...
        │
        └───validation (optional, otherwise validation will be created from images and masks)
        │       └───images
        │       │   val_sequence_or_vol_1
        │       │   val_sequence_or_vol_2
        │       │   ...
        │       │
        │       └───masks
        │           val_sequence_or_vol_1
        │           val_sequence_or_vol_2
        │           ...
        │
        │
        │
        └───test (optional, last 5 epochs will be analyzed against this data)
                └───images
                │   test_sequence_or_vol_1
                │   test_sequence_or_vol_2
                │   ...
                │
                └───masks
                    test_sequence_or_vol_1
                    test_sequence_or_vol_2
                    ...
```
</details>
</details>

<a name="augmentation"></a>
# Offline Augmentation

(see at tutorial for online augmentation)

<details>
<summary><b><u>Click to expand</u></b></summary>

**What it does?**

The term offline augmentation refers to the manipulation of the input data before the training has started. Hereby, 
the operator retains control over the dataset. The augmented images and masks can be inspected by the operator 
beforehand to check if the augmentations are reasonable, as they are notoriously domain depended. An example of a 
classification problem: If the number 6 were rotated by 180°, a 9 would result, but it would still be labeled as a 6.
One disadvantage of this approach is the much larger required storage space.

The augmentation uses many own procedures combined with the great [albumentations](https://pypi.org/project/albumentations/) 
library. Check them out!

**Should I use online or offline augmentation?**

It was found that offline augmentation is superior on fewer data (up to 1k training images), while online augmentation 
is advantageous for a larger dataset (like Cityscapes), as it becomes impractical to store many augmented images on 
the hard drive. Checkout [this paper (link missing, file in review)]().

**How to augment an existing dataset**
1. select augmentation and choose only the images and masks of the training dataset! Do not touch the validation or test data!

The new training data does now consist of:
- original images and masks
- augmented images and masks

### Impact example offline augmentation:
<details>
<summary><b><u>Click to expand</u></b></summary>

This image shows 100 2D neural networks trained to segment water in rivers. Orange: with augmentation, 
Blue: without augmentation.

<img src="/readme_images/validation_accuracy_augmentation_vs_no_aug.PNG" alt="augmentation_impact">

</details>

#### performed 2D Operations (offline):
<details>
<summary><b><u>Click to expand</u></b></summary>

Geometric:
   - elastic distortion
   - flip (height, width)
   - grid distortion
   - grid shuffle
   - optical distortion
   - random crop
   - resize
   - rotation
   - squeeze (height, width)
   - tilt (backward, forward, left, right)

Pixel manipulations (rgb):
   - adaptive histogram
   - blur
   - brightness
   - channel shuffle
   - color to hsv
   - contrast manipulation
   - fog
   - histogram normalization
   - iso noise
   - noise
   - non-local means smoothing
   - rain
   - random erasing
   - rgb shift
   - sharpen
   - snow
   - solarize
   - sun flair
   - to gray
   - to sepia
</details>

#### performed 3D Operations (offline):
<details>
<summary><b><u>Click to expand</u></b></summary>

- base operations:
    - rotate (x, y and z) &#8594; each is performed; rotate by a given angle until a full 360° is reached
    - random resizing on every rotated volume
- additional operations performed on the base volumes:
    - random tilt (tilts every slice of the volume like in 2D)
    - random squeezing along one axis
    - random crop
    - add noise
    - add blur
    - sharpen
    - contrast
    - brightness
    - random shadow
    - random erasing

</details>


#### Info:
<sup>The augmentation will do some operations which might create too small images. In this case the images/volumes are 
not saved and the real number of created samples might be smaller.</sup>

<sup>If you set a maximum number of created images/volumes, for example 250, only a few random operations will be 
executed. However, a rotation will be done in every case. The additional operations will be selected randomly so that 
you get the most out of your images/volumes.</sup>
</details>

<a name="citation"></a>
# Citation
If you use this software or part of its code for your research, I refer to the updated main [project](https://gitlab.com/fra-wa/pytorch_segmentation).
Please cite as stated in section [citation](https://gitlab.com/fra-wa/pytorch_segmentation#citation)

Contact: [Franz Wagner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/photogrammetrie/die-professur/beschaeftigte/m-sc-franz-wagner?set_language=en)


<a name="paper"></a>
# Paper related parameters and more

- Dataset preparation: auto weighting of classes was deactivated. This may improve results even further.
- You can find the RIWA Dataset [here](https://www.kaggle.com/datasets/franzwagner/river-water-segmentation-dataset)
- The pretrained networks regarding KIWA and Cityscapes used for the segmentations as shown below can be found in the 
[pretrained](https://gitlab.com/fra-wa/riwa-segmentation/-/tree/main/pretrained) folder. You can use and or import them as 
explained in the [tutorial](#tutorial) (Import: tutorial -> misc -> import, Use: tutorial -> inference)
- To access all/another pretrained networks, please contact 
[Franz Wagner, TU Dresden](https://tu-dresden.de/bu/umwelt/geo/ipf/photogrammetrie/die-professur/beschaeftigte/m-sc-franz-wagner?set_language=en)


<details>
<summary><b><u>Full network comparison (91 nets) on RIWA</u></b></summary>

Values except batch size represent testing Intersection over Union (IoU)

| Network (Backbone)         | Batch size | Avg       | KIWA No Aug | KIWA Offline Aug | KIWA Online Aug geo first | KIWA Online Aug geo only |
|----------------------------|------------|-----------|-------------|------------------|---------------------------|--------------------------|
| U-Net (ResNeXt 50)         | 24         | **0.940** | **0.928**   | **0.980**        | 0.928                     | 0.922                    |
| GCN (ResNeXt 50)           | 48         | 0.935     | 0.916       | 0.975            | 0.925                     | **0.924**                |
| UPerNet (ResNeXt 50)       | 32         | 0.935     | 0.923       | 0.979            | 0.919                     | 0.921                    |
| GCN (ResNeXt 101)          | 22         | 0.934     | 0.921       | 0.975            | 0.926                     | 0.915                    |
| U-Net (ResNeXt 101)        | 18         | 0.933     | 0.913       | 0.972            | 0.926                     | 0.920                    |
| SegNet (ResNeXt 50)        | 24         | 0.932     | 0.906       | 0.976            | 0.928                     | 0.919                    |
| UPerNet (ResNet 34)        | 96         | 0.932     | 0.908       | 0.979            | 0.925                     | 0.916                    |
| DANet (ResNeXt 50)         | 32         | 0.931     | 0.908       | 0.971            | **0.929**                 | 0.918                    |
| GCN (ResNet 152)           | 32         | 0.931     | 0.916       | 0.970            | 0.923                     | 0.914                    |
| U-Net (ResNet 34)          | 48         | 0.931     | 0.914       | 0.974            | 0.921                     | 0.915                    |
| UPerNet (ResNet 50)        | 40         | 0.930     | 0.913       | 0.976            | 0.918                     | 0.913                    |
| Dran (ResNeXt 50)          | 40         | 0.929     | 0.903       | 0.969            | 0.924                     | 0.922                    |
| GCN (ResNet 18)            | 128        | 0.929     | 0.913       | 0.974            | 0.928                     | 0.902                    |
| OCNet (ResNeXt 101)        | 22         | 0.929     | 0.904       | 0.970            | 0.928                     | 0.915                    |
| PSPDenseNet (DenseNet 121) | 16         | 0.929     | 0.901       | 0.973            | 0.928                     | 0.915                    |
| U-Net (ResNet 18)          | 64         | 0.929     | 0.912       | 0.973            | 0.917                     | 0.916                    |
| GCN (ResNet 34)            | 128        | 0.928     | 0.891       | 0.974            | 0.930                     | 0.916                    |
| PSPDenseNet (DenseNet 169) | 8          | 0.928     | 0.910       | 0.967            | 0.919                     | 0.916                    |
| DANet (ResNeXt 101)        | 22         | 0.927     | 0.906       | 0.970            | 0.926                     | 0.905                    |
| GCN (ResNet 50)            | 64         | 0.927     | 0.909       | 0.970            | 0.920                     | 0.909                    |
| UPerNet (ResNet 18)        | 128        | 0.927     | 0.913       | 0.965            | 0.924                     | 0.908                    |
| UPerNet (ResNet 101)       | 32         | 0.927     | 0.910       | 0.976            | 0.920                     | 0.902                    |
| OCNet (ResNet 50)          | 64         | 0.926     | 0.905       | 0.967            | 0.919                     | 0.914                    |
| GCN (ResNet 101)           | 40         | 0.925     | 0.887       | 0.970            | 0.926                     | 0.919                    |
| OCNet (ResNeXt 50)         | 48         | 0.925     | 0.890       | 0.975            | 0.924                     | 0.912                    |
| SegNet (ResNet 101)        | 22         | 0.925     | 0.897       | 0.964            | 0.923                     | 0.918                    |
| DANet (ResNet 50)          | 64         | 0.924     | 0.889       | 0.973            | 0.923                     | 0.911                    |
| DANet (ResNet 101)         | 40         | 0.923     | 0.891       | 0.962            | 0.924                     | 0.914                    |
| DenseASPP 121              | 18         | 0.923     | 0.884       | 0.972            | 0.920                     | 0.915                    |
| Dran (ResNet 50)           | 48         | 0.923     | 0.894       | 0.972            | 0.925                     | 0.902                    |
| ICNet (ResNeXt 101)        | 64         | 0.923     | 0.890       | 0.969            | 0.922                     | 0.910                    |
| OCNet (ResNet 101)         | 48         | 0.923     | 0.891       | 0.967            | 0.915                     | 0.921                    |
| UPerNet (ResNet 152)       | 24         | 0.923     | 0.904       | 0.974            | 0.915                     | 0.901                    |
| DANet (ResNet 152)         | 40         | 0.922     | 0.901       | 0.965            | 0.923                     | 0.900                    |
| Dran (ResNeXt 101)         | 22         | 0.922     | 0.906       | 0.971            | 0.916                     | 0.895                    |
| PSPDenseNet (DenseNet 201) | 6          | 0.921     | 0.915       | 0.962            | 0.909                     | 0.898                    |
| PSPDenseNet (DenseNet 161) | 6          | 0.921     | 0.902       | 0.967            | 0.915                     | 0.902                    |
| U-Net (ResNet 101)         | 32         | 0.921     | 0.906       | 0.966            | 0.913                     | 0.900                    |
| ICNet (ResNeXt 50)         | 8          | 0.920     | 0.901       | 0.965            | 0.918                     | 0.898                    |
| U-Net 3+ (ResNet 34)       | 12         | 0.919     | 0.904       | 0.972            | 0.908                     | 0.893                    |
| BiSeNet                    | 128        | 0.918     | 0.889       | 0.973            | 0.915                     | 0.894                    |
| DeepLab V3+ (ResNet 101)   | 32         | 0.918     | 0.881       | 0.970            | 0.926                     | 0.895                    |
| DenseASPP 161              | 8          | 0.918     | 0.892       | 0.976            | 0.903                     | 0.900                    |
| U-Net (ResNet 50)          | 40         | 0.918     | 0.889       | 0.963            | 0.914                     | 0.908                    |
| Dran (ResNet 101)          | 32         | 0.917     | 0.888       | 0.973            | 0.920                     | 0.886                    |
| DeepLab V3+ (DRN)          | 22         | 0.916     | 0.884       | 0.971            | 0.913                     | 0.897                    |
| DenseASPP 169              | 10         | 0.915     | 0.886       | 0.974            | 0.914                     | 0.887                    |
| Dran (ResNet 152)          | 24         | 0.915     | 0.878       | 0.965            | 0.919                     | 0.897                    |
| ERFNet                     | 56         | 0.915     | 0.887       | 0.957            | 0.911                     | 0.905                    |
| R2 Attention U-Net         | 5          | 0.915     | 0.891       | 0.970            | 0.898                     | 0.901                    |
| OCNet (ResNet 152)         | 40         | 0.914     | 0.872       | 0.967            | 0.918                     | 0.901                    |
| SegNet (ResNet 50)         | 24         | 0.914     | 0.862       | 0.970            | 0.922                     | 0.902                    |
| Attention U-Net            | 14         | 0.913     | 0.873       | 0.965            | 0.914                     | 0.900                    |
| R2U-Net                    | 6          | 0.913     | 0.886       | 0.975            | 0.907                     | 0.884                    |
| SegNet (ResNet 152)        | 16         | 0.913     | 0.887       | 0.962            | 0.911                     | 0.891                    |
| DenseNet 103               | 4          | 0.912     | 0.885       | 0.958            | 0.911                     | 0.895                    |
| SegNet (ResNeXt 101)       | 12         | 0.912     | 0.876       | 0.970            | 0.914                     | 0.888                    |
| DeepLab V3+ (xception)     | 18         | 0.911     | 0.869       | 0.970            | 0.900                     | 0.906                    |
| DenseNet 67                | 5          | 0.911     | 0.893       | 0.951            | 0.909                     | 0.890                    |
| ENet                       | 64         | 0.911     | 0.886       | 0.956            | 0.906                     | 0.898                    |
| U-Net 3+ (ResNet 50)       | 8          | 0.911     | 0.881       | 0.971            | 0.909                     | 0.882                    |
| U-Net 3+ (ResNet 152)      | 7          | 0.911     | 0.885       | 0.968            | 0.896                     | 0.894                    |
| PSPNet (ResNet 50)         | 32         | 0.910     | 0.877       | 0.969            | 0.918                     | 0.878                    |
| U-Net 3+ (ResNeXt 101)     | 8          | 0.910     | 0.895       | 0.966            | 0.899                     | 0.882                    |
| U-Net (ResNet 152)         | 24         | 0.910     | 0.886       | 0.952            | 0.906                     | 0.897                    |
| HRNet                      | 7          | 0.908     | 0.866       | 0.972            | 0.912                     | 0.882                    |
| U-Net                      | 24         | 0.908     | 0.881       | 0.975            | 0.901                     | 0.876                    |
| U-Net 2+                   | 14         | 0.908     | 0.868       | 0.963            | 0.900                     | 0.900                    |
| U-Net 3+ (ResNet 18)       | 12         | 0.908     | 0.875       | 0.965            | 0.906                     | 0.888                    |
| ICNet (ResNet 101)         | 96         | 0.906     | 0.845       | 0.959            | 0.916                     | 0.903                    |
| DeepLab-DUC-HDC            | 14         | 0.904     | 0.896       | 0.945            | 0.886                     | 0.889                    |
| PSPNet (ResNeXt 50)        | 24         | 0.904     | 0.846       | 0.962            | 0.907                     | 0.902                    |
| PSPNet (ResNeXt 101)       | 18         | 0.904     | 0.873       | 0.951            | 0.909                     | 0.883                    |
| U-Net 3+ (ResNet 101)      | 8          | 0.901     | 0.871       | 0.970            | 0.886                     | 0.879                    |
| DFN                        | 24         | 0.900     | 0.875       | 0.952            | 0.908                     | 0.867                    |
| ICNet (ResNet 152)         | 96         | 0.900     | 0.813       | 0.965            | 0.915                     | 0.906                    |
| ESPNet                     | 128        | 0.898     | 0.871       | 0.938            | 0.893                     | 0.890                    |
| BiSeNetV2                  | 96         | 0.895     | 0.819       | 0.956            | 0.914                     | 0.892                    |
| PSPNet (ResNet 152)        | 24         | 0.895     | 0.884       | 0.922            | 0.907                     | 0.868                    |
| DenseNet 57                | 12         | 0.891     | 0.881       | 0.924            | 0.892                     | 0.866                    |
| U-Net 3+ (ResNeXt 50)      | 8          | 0.890     | 0.810       | 0.965            | 0.901                     | 0.883                    |
| ICNet (ResNet 50)          | 128        | 0.889     | 0.770       | 0.963            | 0.914                     | 0.911                    |
| SINet                      | 128        | 0.885     | 0.844       | 0.930            | 0.888                     | 0.880                    |
| PSPNet (ResNet 101)        | 24         | 0.884     | 0.843       | 0.960            | 0.864                     | 0.869                    |
| ExtremeC3Net               | 64         | 0.880     | 0.851       | 0.918            | 0.888                     | 0.863                    |
| DFANet                     | 64         | 0.870     | 0.784       | 0.951            | 0.886                     | 0.858                    |
| FastSCNN                   | 128        | 0.867     | 0.770       | 0.930            | 0.890                     | 0.876                    |
| LadderNet                  | 40         | 0.854     | 0.811       | 0.909            | 0.869                     | 0.827                    |
| FCN 8                      | 32         | 0.850     | 0.816       | 0.876            | 0.897                     | 0.810                    |
| LEDNet                     | 64         | 0.841     | 0.828       | 0.893            | 0.826                     | 0.815                    |
| FCN 16                     | 24         | 0.840     | 0.775       | 0.868            | 0.852                     | 0.864                    |

</details>

<details>
<summary><b><u>Parameters used for online augmentation</u></b></summary>

**RIWA Online Augmentation Parameters**

Parameters and approximated activation probabilities of the online augmentation regarding RIWA:

<details>
<summary><b><u>Click to expand</u></b></summary>

<img src="/readme_images/paper_related/riwa_para_onl_aug_approximated.png" alt="augmentation_impact">

</details>

Parameters and intuitive activation probabilities of the online augmentation regarding RIWA:

<details>
<summary><b><u>Click to expand</u></b></summary>

<img src="/readme_images/paper_related/riwa_para_onl_aug_intuitive.png" alt="augmentation_impact">

</details>

**Cityscapes Online Augmentation Parameters**

Parameters and approximated activation probabilities of the online augmentation regarding Cityscapes:

<details>
<summary><b><u>Click to expand</u></b></summary>

<img src="/readme_images/paper_related/city_para_onl_aug_approximated.png" alt="augmentation_impact">

</details>

Parameters and intuitive activation probabilities of the online augmentation regarding Cityscapes:

<details>
<summary><b><u>Click to expand</u></b></summary>

<img src="/readme_images/paper_related/city_para_onl_aug_intuitive.png" alt="augmentation_impact">

</details>

</details>

**Example Segmentations:**

---
<img src="/readme_images/paper_related/city_examples.jpg" alt="augmentation_impact">
Results of a segmentation using the Cityscapes dataset with online augmentation (geo first, n=8 - see parameters above) and the U-Net (ResNeXt 50). Left:
Input image; Center: Segmentation as overlay; Right: Difference map between the ground truth and the segmentation
(green: segmentation = ground truth, red: segmentation != ground truth).

---

<img src="/readme_images/paper_related/riwa_examples.jpg" alt="augmentation_impact">
Results of a segmentation using the offline augmented RIWA dataset and the U-Net (ResNeXt 50). Left:
Input image; Center: Segmentation as overlay; Right: Difference map between the ground truth and the segmentation
(green: segmentation = ground truth, red: segmentation != ground truth).

---
